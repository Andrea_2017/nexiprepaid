import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './css/main.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
  Link
} from 'react-router-dom';
import {App} from './App.js';
import {Main} from './Main.js';
import {Vendita} from './Vendita.js';
import {Gestione} from './Gestione.js';
import {Informazioni} from './Informazioni.js';
import {Errore} from './Errore.js';
import {ShowTheLocation} from './Tester.js';
import {Popup} from './Popup.js';
import {getBaseUrl} from './Utils.js';

ReactDOM.render(
  <BrowserRouter basename={getBaseUrl()}>
    <Main />
  </BrowserRouter>, document.getElementById('root'));

{/*
ReactDOM.render(
  <BrowserRouter basename='prepaid'>
    <Main />
  </BrowserRouter>, document.getElementById('root'));
*/}

{/*}
ReactDOM.render(
  <BrowserRouter basename='prepaid'>
    <Errore />
  </BrowserRouter>, document.getElementById('root'));
*/}