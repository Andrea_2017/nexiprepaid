import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {PATHS, TIPOLOGIE_CARTA, URL_CARTA, MESSAGE, RICERCA} from './Constants';
import {setToString, MessageException, hasValue, getBaseUrl} from './Utils';

/*class Forms extends React.Component {
  constructor(props) {
    console.log('Form.constructor');
    super(props);
    this.state = {
      cardType: 'contrattualizzata'
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log('Form.handleChange -> ' +
                '[event.target.value=' + event.target.value +
                ', event.target.type=' + event.target.type +
                ', event.target.checked=' + event.target.checked + ']');

    this.setState({
      cardType: event.target.value
    });
  }

  handleSubmit(event) {
    console.log('Form.handleSubmit -> ' +
                '[event.target.value=' + event.target.value +
                ', event.target.type=' + event.target.type + ']');
    event.preventDefault();
  }

  render() {
    const cardType = this.state.cardType;
    console.log('Form.render -> ' +
                '[cardType=' + cardType +
                ', contrattualizzata=' + (cardType === 'contrattualizzata') +
                ', contrattualizzataMinore=' + (cardType === 'contrattualizzataMinore') +
                ', nonContrattualizzata=' + (cardType === 'nonContrattualizzata') + ']');

	  return (
      <div>
        <div>
          <input type='radio' onChange={this.handleChange} name='cardtype' value='contrattualizzata' checked={(cardType === 'contrattualizzata') ? true : false} />Carta contrattualizzata
        </div>
        <div>
          <input type='radio' onChange={this.handleChange} name='cardtype' value='contrattualizzataMinore' checked={(cardType === 'contrattualizzataMinore') ? true : false} />Carta contrattualizzata per minore
        </div>
        <div>
          <input type='radio' onChange={this.handleChange} name='cardtype' value='nonContrattualizzata' checked={(cardType === 'nonContrattualizzata') ? true : false} />Carta non contrattualizzata (max 3 a persona)
        </div>
        <p>
          <Link to={'/richiesta?type='+cardType} className='fright'>Conferma</Link>
        </p>
      </div>
	  );
  }
}*/

class CreditCard extends React.Component {
  constructor(props) {
    super(props);
    let {disabled = true} =props;
    this.state = {value: '', disabled: disabled};
  }

 validate(){
   var visaRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
   var mastercardRegEx = /^(?:5[1-5][0-9]{14})$/;
   var amexpRegEx = /^(?:3[47][0-9]{13})$/;
   var discovRegEx = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
   let ccNum = this.pan1.value + this.pan2.value + this.pan3.value + this.pan4.value;

   if ( this.state.disabled ){ // Validazione Visa
     return;
       }
  else if (!this.state.disabled && visaRegEx.test(ccNum) === false){
     alert("Il numero di carta non è valido");
     throw new Error ('Credit card error');
  }
  else {
     alert("Il numero di carta è corretto");
       }
 }

 reset(){
   let ccNum = this.pan1.value + this.pan2.value + this.pan3.value + this.pan4.value;
   if (ccNum !== '') {
    this.pan1.value  =  ''
    this.pan2.value  =  ''
    this.pan3.value  =  ''
    this.pan4.value  =  ''
   } else {
     return;
   }
 }

  render() {
    const search = this.props.search;
    const disable= this.state.disabled;
    return(
      <td>
        <input type='text' maxLength={4} size="4"  name='pan1' ref={(input) => { this.pan1 = input; }} value={this.state.pan1} disabled = {(this.state.disabled)? "disabled" : ""}/>
        &nbsp;
        <input type='text' maxLength={4} size="4"  name='pan2' ref={(input) => { this.pan2 = input; }} value={this.state.pan2} disabled = {(this.state.disabled)? "disabled" : ""}/>
        &nbsp;
        <input type='text' maxLength={4} size="4"  name='pan3' ref={(input) => { this.pan3 = input; }} value={this.state.pan3} disabled = {(this.state.disabled)? "disabled" : ""}/>
        &nbsp;
        <input type='text' maxLength={4} size="4"  name='pan4' ref={(input) => { this.pan4 = input; }} value={this.state.pan4} disabled = {(this.state.disabled)? "disabled" : ""}/>
      </td>
  );
  }
}

class CodiceFiscale extends React.Component {
  constructor(props) {
    super(props);
    let {disabled = true} =props;
    this.state = {value: '', disabled: disabled};
  }
 validate(){
   var codice_fiscale = /^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/i;
   let codfis = this.cf.value;

   if ( this.state.disabled ){ // Validazione codice fiscale
       return;
       }
   else if ( !this.state.disabled && codice_fiscale.test(codfis) === false) {
      alert("Codice fiscale non corretto");
      throw new Error ('Codice fiscale error');
    }
   else{
        alert("Il codice fiscale è corretto");
    }
 }

 reset(){
   let codfis = this.cf.value;
   if (codfis !== '') {
     this.cf.value=  ''
   } else {
     return;
   }
 }

  render() {
    const search = this.props.search;
    const disable= this.state.disabled;
    const style = {
        textTransform: 'uppercase'
      };
    return(
      <td>
      <input type='text' maxLength={16} onChange={this.handleChange} name='cf' ref={(input) => { this.cf = input; }} value={this.state.cf} disabled = {(this.state.disabled)? "disabled" : ""} style={style}/>
      </td>
  );
  }
}

class Nome extends React.Component {
  constructor(props) {
    super(props);
    let {disabled = true} =props;
    this.state = {value: '', disabled: disabled};
  }
 validate(){
   var nascita = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/;
   let  birth = this.nascita.value;

   if ( this.state.disabled ){ // Validazione codice fiscale
       return;
       }
   else if ( !this.state.disabled && nascita.test(birth) === false) {   // Validazione data di nascita
      alert("La data di nascita non è corretta (formato DD/MM/YYYY)");
      throw new Error ('Data nascita error');
    }
   else{
        alert("La data di nascita è corretta");
    }
 }

 reset(){
   let  birth = this.nascita.value + this.nome.value;
   if (birth !== '') {
    this.nascita.value = ''
    this.nome.value =  ''
   } else {
     return;
   }
 }

  render() {
    const search = this.props.search;
    const disable= this.state.disabled;
    return(
      <td>
        <input type='text'  name='nome' ref={(input) => { this.nome = input; }} value={this.state.nome} disabled = {(this.state.disabled)? "disabled" : ""}/>
        &nbsp;
        Data di nascita
        <input type='text' maxLength={10} name='nascita' ref={(input) => { this.nascita = input; }} value={this.state.nascita} disabled = {(this.state.disabled)? "disabled" : ""}/>
      </td>
  );
  }
}

class Posizione extends React.Component {
  constructor(props) {
    super(props);
    let {disabled = true} =props;
    this.state = {value: '', disabled: disabled};
  }
 validate(){
   var position = /^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/i; // da verificare
   let pos = this.posizione.value;

   if ( this.state.disabled ){ // Validazione codice fiscale
       return;
       }
   else if ( !this.state.disabled && position.test(pos) === false) {   // Validazione posizione
      alert("Posizione non corretta");
      throw new Error ('Position error');
    }
   else{
        alert("La posizione è corretta");
    }
 }

 reset(){
   let pos = this.posizione.value;
   if (pos !== '') {
     this.posizione.value =  ''
   } else {
     return;
   }
 }

  render() {
    const search = this.props.search;
    const disable= this.state.disabled;
    return(
      <td>
      <input type='text' onChange={this.handleChange}  name='posizione' ref={(input) => { this.posizione = input; }} value={this.state.posizione} disabled = {(this.state.disabled)? "disabled" : ""} />
      </td>
  );
  }
}



class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleChange = this.handleChange.bind(this);
  }

 validate(){
   try {
     this.creditcard.validate();
     this.codicefiscale.validate();
     this.nascita.validate();
     this.posizione.validate();
   }
   catch (e){
     alert('catch' + e);
   }
 }

 reset(){
   this.creditcard.reset();
   this.codicefiscale.reset();
   this.nascita.reset();
   this.posizione.reset();
 }

  handleChange(event) {
    let { value } = event.target;
      this.creditcard.setState({disabled:value != RICERCA['NUMERO_CARTA']});
      this.codicefiscale.setState({disabled:value != RICERCA['CODICE_FISCALE']});
      this.nascita.setState({disabled:value != RICERCA['NOME_COGNOME']});
      this.posizione.setState({disabled:value != RICERCA['NUMERO_POSIZIONE']});
  }

  render() {
      const search = this.props.search;
      const disable = this.props.disable;

	  return (
      <table>
        <tbody>
        <tr>
          <td>
            <input type='radio' onChange={this.handleChange} name='search' value={RICERCA['NUMERO_CARTA']} />Numero di carta
          </td>
            <CreditCard search= 'NUMERO_CARTA' disabled= {false} ref={(input) => { this.creditcard = input; }}/>
        </tr>
        <tr>
          <td>
            <input type='radio' onChange={this.handleChange} name='search' value={RICERCA['CODICE_FISCALE']}  />Codice fiscale
          </td>
            <CodiceFiscale search= 'CODICE_FISCALE' disabled= {false} ref={(input) => { this.codicefiscale = input; }}/>
        </tr>
        <tr>
          <td>
            <input type='radio' onChange={this.handleChange} name='search' value={RICERCA['NOME_COGNOME']} />Nome e cognome
          </td>
           <Nome search= 'NOME_COGNOME' disabled= {false} ref={(input) => { this.nascita = input; }}/>
        </tr>
        <tr>
          <td>
            <input type='radio' onChange={this.handleChange} name='search' value={RICERCA['NUMERO_POSIZIONE']} />Numero posizione
          </td>
            <Posizione search= 'NUMERO_POSIZIONE' disabled= {false} ref={(input) => { this.posizione = input; }}/>
        </tr>
        </tbody>
      </table>
	  );
  }
}

export class GestioneWoRouter extends React.Component {
  constructor(props) {
    console.log('Gestione.constructor');
    super(props);

    const { match, location, history } = this.props;
    this.state = {
      search: RICERCA['NUMERO_CARTA'],
      position: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAnnulla = this.handleAnnulla.bind(this);
  }

  handleSubmit(event) {

    this.formsearch.validate();
    event.preventDefault();
    /*let url= `data/${id}/${year}/${month}.json` ;
    fetch(url).then(function(response) {
      if (response.ok) {
        return response.json();
      }
      throw new Error ('');
    })
    .then ((dataJson)=>{

    })
    .catch(function(error){

    })*/
  }

  handleAnnulla(event) {
    this.formsearch.reset();
  }

  render() {
    console.log('Gestione.render');
    return (
      <div className='column'>
        <h2>Gestione carta - Ricerca</h2>
        <p>Seleziona la modalità di ricerca tra quelle proposte</p>
        <Form search={this.state.search} ref={(input)=>this.formsearch=input} />
        <p>
          <a onClick={this.handleAnnulla} className='fright'>Annulla</a>
        </p>
        <p>
          <a onClick={this.handleSubmit} className='fright'>Conferma</a>
        </p>
      </div>
    );
  }
}

export const Gestione = withRouter(GestioneWoRouter)
