import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {PATHS, TIPOLOGIE_CARTA, URL_CARTA, MESSAGE} from './Constants';
import {Modal, Button, AlertContainer} from 'react-bootstrap';
import {setToString, MessageException, hasValue, getBaseUrl} from './Utils';

class Popup extends React.Component {
  render () {
    return (
      <div className="static-modal">
        <Modal.Dialog>
          <Modal.Body>
            <p>
              Prima di proseguire con la richiesta carta, inserisci nel box sotto
              il n. posizione che trovi all'interno della fustella e clicca Conferma
            </p>
            <div className='popup'>
              <input type='text' onChange={this.props.handleChange} />
            </div>
          </Modal.Body>
          <Modal.Footer>
          <button className='annullaButton' onClick={this.props.handleAnnulla}>
            Annulla
          </button>
          <button className='confermaButton' onClick={this.props.handleProsegui}>
            Conferma
          </button>
          </Modal.Footer>
        </Modal.Dialog>
      </div>
    )
  }
}

class Form extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const cardType = this.props.cardType;

	  return (
      <div>
        <div>
          <input type='radio' onChange={this.props.handleChange} name='cardtype' value={TIPOLOGIE_CARTA['CONTRATTUALIZZATA']} checked={(cardType === TIPOLOGIE_CARTA['CONTRATTUALIZZATA']) ? true : false} />Carta contrattualizzata
        </div>
        <div>
          <input type='radio' onChange={this.props.handleChange} name='cardtype' value={TIPOLOGIE_CARTA['CONTRATTUALIZZATA_MINORE']} checked={(cardType === TIPOLOGIE_CARTA['CONTRATTUALIZZATA_MINORE']) ? true : false} />Carta contrattualizzata per minore
        </div>
        <div>
          <input type='radio' onChange={this.props.handleChange} name='cardtype' value={TIPOLOGIE_CARTA['NON_CONTRATTUALIZZATA']} checked={(cardType === TIPOLOGIE_CARTA['NON_CONTRATTUALIZZATA']) ? true : false} />Carta non contrattualizzata (max 3 a persona)
        </div>
      </div>
	  );
  }
}

class VenditaWoRouter extends React.Component {
  constructor(props) {
    super(props);
    const { match, location, history } = this.props;
    this.state = {
      cardType: TIPOLOGIE_CARTA['CONTRATTUALIZZATA'],
      showPopup: false,
      position: ''
    };
    this.handleChangeCardType = this.handleChangeCardType.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAnnulla = this.handleAnnulla.bind(this);
    this.handleProsegui = this.handleProsegui.bind(this);
    this.handleChangePosition = this.handleChangePosition.bind(this);
  }

  handleChangeCardType(event) {
    this.setState({
      cardType: event.target.value
    });
  }

  handleChangePosition(event) {
    this.setState({
      position: event.target.value
    });
  }

  handleAnnulla(event) {
    event.preventDefault();
    this.setState({
      showPopup: false
    });
  }

  handleProsegui(event) {
    event.preventDefault();
    var parameters =
      'applicazione=' + 'PORTALE_BANCHE' + '&'
    + 'utente=' + 'ka99999' + '&'
    + 'canale=' + 'WEB' + '&'
    + 'abi=' + '03434' + '&'
    + 'posizione=' + this.state.position + '&'
    + 'tipologia_carta=' + this.state.cardType

    console.log('parameters=>' + parameters);
    fetch(getBaseUrl() + '/verificaPosizione.do', {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: parameters
    })
    .then(response => {
      console.log("response");
      var json = response.json();
      if (!response.ok) {
        if (hasValue(MESSAGE[response.status]))
          throw {titolo: MESSAGE[response.status].titolo, messaggio: MESSAGE[response.status].messaggio}
      }
      return json;
    })
    .then(data => {
      this.props.handleRedirect(PATHS[this.state.cardType]);
    })
    .catch(error => {
      if (hasValue(error.titolo) && hasValue(error.messaggio)) {
        console.log('errore con valori');
        this.props.handleRedirect(PATHS['errore'], {titolo: error.titolo, messaggio: error.messaggio});
      } else {
        console.log('errore senza valori');
        this.props.handleRedirect(PATHS['errore']);
      }
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      showPopup: true
    });
  }

  render() {
    console.log(this.props.location);
    return (
      <div className='column columnTight'>
        <h2>Vendita</h2>
        <p>
          Seleziona la tipologia di carta più adatta:
        </p>
        <Form cardType={this.state.cardType} handleChange={this.handleChangeCardType} />
        <p>
          <a onClick={this.handleSubmit} className='fright'>Conferma</a>
        </p>
        {(this.state.showPopup) ? <Popup handleChange={this.handleChangePosition} handleAnnulla={this.handleAnnulla} handleProsegui={this.handleProsegui} /> : null}
      </div>
    );
  }
}

export const Vendita = withRouter(VenditaWoRouter)
