import React from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import stepbar from './images/stepbar.gif';
import imgNexiPrepaidFronte from './images/nexi_prepaid_fronte.png';
import {
  PaginaInformativa,
  FORM_IDS as IDS_INFORMATIVA} from './PaginaInformativa';
import {
  FormDatiPersonali,
  FORM_IDS as IDS_DATI_PERSONALI,
  validaCodiceFiscale,
  validaMaggiorenne,
  validaMinorenne,
  validaEmail,
  validaAnnoRilascioDocumento,
  validaAnnoScadenzaDocumento,
  validaDataEmissioneDocumento,
  validaDataScadenzaDocumento} from './FormDatiPersonali';
import {
  FormDatiProfessionali,
  FORM_IDS as IDS_DATI_PROFESSIONALI,
  FORM_VALUES as VALUES_DATI_PROFESSIONALI} from './FormDatiProfessionali';
import {
  FormRiepilogo,
  FORM_IDS as IDS_RIEPILOGO,
  FORM_VALUES as VALUES_IDS_RIEPILOGO} from './FormRiepilogo';
  import {Finalizzazione} from './Finalizzazione';
import {TIPOLOGIE_CARTA, MESSAGE, ESITO, PATHS} from './Constants';
import {setToString, MessageException, hasValue, getBaseUrl} from './Utils';
import {TestCss} from './TestCss';
import {FormErrors} from './FormErrors';

export const FIELD_STATUS = {
  TO_CHECK: 'field_to_check',
  OK: 'field_validated',
  KO: 'field_not_validated'
};

export function retrieveFondiValues(fondi) {
  var provenienze = fondi.provenienze;
  if (provenienze.has(VALUES_DATI_PROFESSIONALI['PROVENIENZA_FONDI'].altro)) {
    provenienze.delete(VALUES_DATI_PROFESSIONALI['PROVENIENZA_FONDI'].altro);
    provenienze.add(fondi.altraProvenienza);
  }
  return Array.from(provenienze);
}

const STEP_CONFIGURATION = {
  INFORMATIVA: { stepbarNumber: 1,
                    navigateBack: false,
                    navigateNext: true},
  DATI_PERSONALI: {stepbarNumber: 2,
                      navigateBack: true,
                      navigateNext: true},
  DATI_PROFESSIONALI: {stepbarNumber: 2,
                          navigateBack: true,
                          navigateNext: true},
  RIEPILOGO: { stepbarNumber: 3,
                  navigateBack: true,
                  navigateNext: true},
  FINALIZZAZIONE: {stepbarNumber: 4,
                      navigateBack: false,
                      navigateNext: false},
  FINE: {stepbarNumber: 4,
            navigateBack: false,
            navigateNext: false}
};

const STEP_CARTA = {
  contrattualizzata: [
    'INFORMATIVA',
    'DATI_PERSONALI',
    'DATI_PROFESSIONALI',
    'RIEPILOGO',
    'FINALIZZAZIONE',
    'FINE'
  ],
  contrattualizzata_minore: [
    'INFORMATIVA',
    'DATI_PERSONALI',
    'DATI_PROFESSIONALI',
    'RIEPILOGO',
    'FINALIZZAZIONE',
    'FINE'
  ],
  non_contrattualizzata: [
    'INFORMATIVA',
    'DATI_PERSONALI',
    'RIEPILOGO',
    'FINALIZZAZIONE',
    'FINE'
  ]
};

const STEPBAR_LABELS = [
  { number: 1,
    text: 'Informativa-precontrattuale'
  },
  { number: 2,
    text: 'Dati personali'
  },
  { number: 3,
    text: 'Riepilogo'
  },
  { number: 4,
    text: 'Finalizzazione'
  }
];

const OLD_HTTP_PARAMETERS = {
  TIPOLOGIA_STEP: {nome: 'tipologia_step',
                   label: ''},
  TIPOLOGIA_RICHIESTA_CARTA: {nome: 'tipologia_richiesta_carta',
                              label: ''},
  CONSENSO_INFORMATIVA: {nome: 'consenso_informativa',
                         label: 'Ho preso visione dell\'informativa pre-contrattuale'},
  CONSENSO_TRATTAMENTO_DATI_PERSONALI: {nome: 'consenso_trattamento_dati_personali',
                                        label: 'Presto il consenso'},
  CONSENSO_C3: {nome: 'consenso_c3',
                label: ''},
  CONSENSO_C4: {nome: 'consenso_c4',
                label: ''},
  CONSENSO_C5: {nome: 'consenso_c5',
                label: ''},
  TITOLARE_INFORMAZIONI_PERSONALI_NOME: {nome: 'titolare_informazioni_personali_nome',
                                         label: 'Nome'},
  TITOLARE_INFORMAZIONI_PERSONALI_COGNOME: {nome: 'titolare_informazioni_personali_cognome',
                                            label: 'Cognome'},
  TITOLARE_INFORMAZIONI_PERSONALI_SESSO: {nome: 'titolare_informazioni_personali_sesso',
                                          label: 'Sesso'},
  TITOLARE_INFORMAZIONI_PERSONALI_NASCITA: {nome: 'titolare_informazioni_personali_nascita',
                                            label: 'Data di nascita'},
  TITOLARE_INFORMAZIONI_PERSONALI_NASCITA_GIORNO: {nome: 'titolare_informazioni_personali_nascita_giorno',
                                                   label: 'Giorno di nascita'},
  TITOLARE_INFORMAZIONI_PERSONALI_NASCITA_MESE: {nome: 'titolare_informazioni_personali_nascita_mese',
                                                 label: 'Mese di nascita'},
  TITOLARE_INFORMAZIONI_PERSONALI_NASCITA_ANNO: {nome: 'titolare_informazioni_personali_nascita_anno',
                                                 label: 'Anno di nascita'},
  TITOLARE_INFORMAZIONI_PERSONALI_NASCITA_NAZIONE: {nome: 'titolare_informazioni_personali_nascita_nazione',
                                                    label: 'Nazione di nascita'},
  TITOLARE_INFORMAZIONI_PERSONALI_NASCITA_PROVINCIA: {nome: 'titolare_informazioni_personali_nascita_provincia',
                                                      label: 'Provincia di nascita'},
  TITOLARE_INFORMAZIONI_PERSONALI_NASCITA_LOCALITA: {nome: 'titolare_informazioni_personali_nascita_localita',
                                                     label: 'Localit&agrave; di nascita'},
  TITOLARE_INFORMAZIONI_PERSONALI_CODICE_FISCALE: {nome: 'titolare_informazioni_personali_codice_fiscale',
                                                   label: 'Codice fiscale'},
  TITOLARE_INFORMAZIONI_PERSONALI_CITTADINANZA: {nome: 'titolare_informazioni_personali_cittadinanza',
                                                 label: 'Cittadinanza'},
  TITOLARE_CONTATTI_EMAIL: {nome: 'titolare_contatti_email',
                            label: 'Email'},
  TITOLARE_CONTATTI_RIPETI_EMAIL: {nome: 'titolare_contatti_ripeti_email',
                                   label: 'Ripeti Email'},
  TITOLARE_CONTATTI_TELEFONO_CELLULARE: {nome: 'titolare_contatti_telefono_cellulare',
                                         label: 'Telefono cellulare'},
  TITOLARE_CONTATTI_TELEFONO_ABITAZIONE: {nome: 'titolare_contatti_telefono_abitazione',
                                          label: 'Telefono abitazione'},
  TITOLARE_CONTATTI_TELEFONO_UFFICIO: {nome: 'titolare_contatti_telefono_ufficio',
                                       label: 'Telefono ufficio'},
  TITOLARE_RESIDENZA_NAZIONE: {nome: 'titolare_residenza_nazione',
                               label: 'Nazione di residenza'},
  TITOLARE_RESIDENZA_PROVINCIA: {nome: 'titolare_residenza_provincia',
                                 label: 'Provincia di nascita'},
  TITOLARE_RESIDENZA_LOCALITA: {nome: 'titolare_residenza_localita',
                                label: 'Localit&agrave; di nascita'},
  TITOLARE_RESIDENZA_CAP: {nome: 'titolare_residenza_cap',
                           label: 'CAP residenza'},
  TITOLARE_RESIDENZA_RECAPITO_TIPO: {nome: 'titolare_residenza_recapito_tipo',
                                     label: 'Tipo indirizzo di residenza'},
  TITOLARE_RESIDENZA_RECAPITO_INDIRIZZO: {nome: 'titolare_residenza_recapito_indirizzo',
                                          label: 'Indirizzo di residenza'},
  TITOLARE_RESIDENZA_RECAPITO_NUMERO: {nome: 'titolare_residenza_recapito_numero',
                                       label: 'Numero indirizzo di residenza'},
  TITOLARE_RESIDENZA_PRESSO: {nome: 'titolare_residenza_presso',
                              label: 'Presso (residenza)'},
  TITOLARE_RESIDENZA_FISCALE_NAZIONE1: {nome: 'titolare_residenza_fiscale_nazione1',
                                        label: 'Nazione di residenza fiscale (scelta 1)'},
  TITOLARE_RESIDENZA_FISCALE_CODICE_FISCALE_ESTERO1: {nome: 'titolare_residenza_fiscale_codice_fiscale_estero1',
                                                      label: 'Codice fiscale estero (scelta 1)'},
  TITOLARE_RESIDENZA_FISCALE_NAZIONE2: {nome: 'titolare_residenza_fiscale_nazione2',
                                        label: 'Nazione di residenza fiscale (scelta 2)'},
  TITOLARE_RESIDENZA_FISCALE_CODICE_FISCALE_ESTERO2: {nome: 'titolare_residenza_fiscale_codice_fiscale_estero2',
                                                      label: 'Codice fiscale estero (scelta 2)'},
  TITOLARE_RESIDENZA_FISCALE_NAZIONE3: {nome: 'titolare_residenza_fiscale_nazione3',
                                        label: 'Nazione di residenza fiscale (scelta 3)'},
  TITOLARE_RESIDENZA_FISCALE_CODICE_FISCALE_ESTERO3: {nome: 'titolare_residenza_fiscale_codice_fiscale_estero3',
                                                      label: 'Codice fiscale estero (scelta 3)'},
  TITOLARE_DOMICILIO_NAZIONE: {nome: 'titolare_domicilio_nazione',
                               label: 'Nazione di domicilio'},
  TITOLARE_DOMICILIO_PROVINCIA: {nome: 'titolare_domicilio_provincia',
                                 label: 'Provincia di domicilio'},
  TITOLARE_DOMICILIO_LOCALITA: {nome: 'titolare_domicilio_localita',
                                label: 'Localit&agrave; di domicilio'},
  TITOLARE_DOMICILIO_CAP: {nome: 'titolare_domicilio_cap',
                           label: 'CAP domicilio'},
  TITOLARE_DOMICILIO_RECAPITO_TIPO: {nome: 'titolare_domicilio_recapito_tipo',
                                     label: 'Tipo indirizzo di domicilio'},
  TITOLARE_DOMICILIO_RECAPITO_INDIRIZZO: {nome: 'titolare_domicilio_recapito_indirizzo',
                                          label: 'Indirizzo di domicilio'},
  TITOLARE_DOMICILIO_RECAPITO_NUMERO: {nome: 'titolare_domicilio_recapito_numero',
                                       label: 'Numero indirizzo di domicilio'},
  TITOLARE_DOMICILIO_PRESSO: {nome: 'titolare_domicilio_presso',
                              label: 'Presso (domicilio)'},
  TITOLARE_DOCUMENTO_TIPO: {nome: 'titolare_documento_tipo',
                            label: 'Tipo documento'},
  TITOLARE_DOCUMENTO_NUMERO: {nome: 'titolare_documento_numero',
                              label: 'Numero di documento'},
  TITOLARE_DOCUMENTO_AUTORITA: {nome: 'titolare_documento_autorita',
                                label: 'Autorit&agrave; rilascio documento'},
  TITOLARE_DOCUMENTO_NAZIONE: {nome: 'titolare_documento_nazione',
                               label: 'Nazione di rilascio documento'},
  TITOLARE_DOCUMENTO_PROVINCIA: {nome: 'titolare_documento_provincia',
                                 label: 'Provincia di rilascio documento'},
  TITOLARE_DOCUMENTO_LOCALITA: {nome: 'titolare_documento_localita',
                                label: 'Localit&agrave; di rilascio documento'},
  TITOLARE_DOCUMENTO_EMISSIONE: {nome: 'titolare_documento_emissione',
                                 label: 'Data di emissione documento'},
  TITOLARE_DOCUMENTO_EMISSIONE_GIORNO: {nome: 'titolare_documento_emissione_giorno',
                                        label: 'Giorno di emissione documento'},
  TITOLARE_DOCUMENTO_EMISSIONE_MESE: {nome: 'titolare_documento_emissione_mese',
                                      label: 'Mese di emissione documento'},
  TITOLARE_DOCUMENTO_EMISSIONE_ANNO: {nome: 'titolare_documento_emissione_anno',
                                      label: 'Anno di rilascio documento'},
  TITOLARE_DOCUMENTO_SCADENZA: {nome: 'titolare_documento_scadenza',
                                label: 'Data di scadenza documento'},
  TITOLARE_DOCUMENTO_SCADENZA_GIORNO: {nome: 'titolare_documento_scadenza_giorno',
                                       label: 'Giorno di scadenza documento'},
  TITOLARE_DOCUMENTO_SCADENZA_MESE: {nome: 'titolare_documento_scadenza_mese',
                                     label: 'Mese di scadenza documento'},
  TITOLARE_DOCUMENTO_SCADENZA_ANNO: {nome: 'titolare_documento_scadenza_anno',
                                     label: 'Anno di scadenza documento'},
  TITOLARE_DATI_PROFESSIONALI_ABI: {nome: 'titolare_dati_professionali_abi',
                                    label: 'Abi'},
  TITOLARE_DATI_PROFESSIONALI_CAB: {nome: 'titolare_dati_professionali_cab',
                                    label: 'Cab'},
  TITOLARE_DATI_PROFESSIONALI_AGENZIA: {nome: 'titolare_dati_professionali_agenzia',
                                        label: 'Agenzia'},
  TITOLARE_DATI_PROFESSIONALI_CIN: {nome: 'titolare_dati_professionali_cin',
                                    label: 'Cin'},
  TITOLARE_DATI_PROFESSIONALI_PROFESSIONE: {nome: 'titolare_dati_professionali_professione',
                                            label: 'Professione'},
  TITOLARE_DATI_PROFESSIONALI_FASCIA_REDDITO: {nome: 'titolare_dati_professionali_fascia_reddito',
                                               label: 'Reddito'},
  TITOLARE_DATI_PROFESSIONALI_TIPO_ATTIVITA_ECONOMICA: {nome: 'titolare_dati_professionali_tipo_attivita_economica',
                                                        label: 'Tipo attivit&agrave; economica'},
  TITOLARE_DATI_PROFESSIONALI_DATORE_LAVORO: {nome: 'titolare_dati_professionali_datore_lavoro',
                                              label: 'Datore di lavoro'},
  TITOLARE_DATI_PROFESSIONALI_PROVINCIA: {nome: 'titolare_dati_professionali_provincia',
                                          label: 'Provincia'},
  TITOLARE_DATI_PROFESSIONALI_NAZIONE: {nome: 'titolare_dati_professionali_nazione',
                                        label: 'Nazione'},
  TITOLARE_DATI_PROFESSIONALI_FONDI: {nome: 'titolare_dati_professionali_fondi',
                                      label: 'Fondi'},
  TITOLARE_DATI_PROFESSIONALI_FONDI_ALTRA_PROVENIENZA: {nome: 'titolare_dati_professionali_fondi_altra_provenienza',
                                                        label: 'Altra provenienza'},
  TITOLARE_DATI_PROFESSIONALI_FINALITA_RAPPORTO: {nome: 'titolare_dati_professionali_finalita_rapporto',
                                                  label: 'Finalit&agrave; del rapporto'},
  TITOLARE_DATI_PROFESSIONALI_ESPOSIZIONE_POLITICA: {nome: 'titolare_dati_professionali_esposizione_politica',
                                                     label: 'Esposizione politica'},
  TUTORE_INFORMAZIONI_PERSONALI_NOME: {nome: 'tutore_informazioni_personali_nome',
                                       label: 'Nome'},
  TUTORE_INFORMAZIONI_PERSONALI_COGNOME: {nome: 'tutore_informazioni_personali_cognome',
                                          label: 'Cognome'},
  TUTORE_INFORMAZIONI_PERSONALI_SESSO: {nome: 'tutore_informazioni_personali_sesso',
                                        label: 'Sesso'},
  TUTORE_INFORMAZIONI_PERSONALI_NASCITA: {nome: 'tutore_informazioni_personali_nascita',
                                          label: 'Data di nascita'},
  TUTORE_INFORMAZIONI_PERSONALI_NASCITA_GIORNO: {nome: 'tutore_informazioni_personali_nascita_giorno',
                                                 label: 'Giorno di nascita'},
  TUTORE_INFORMAZIONI_PERSONALI_NASCITA_MESE: {nome: 'tutore_informazioni_personali_nascita_mese',
                                               label: 'Mese di nascita'},
  TUTORE_INFORMAZIONI_PERSONALI_NASCITA_ANNO: {nome: 'tutore_informazioni_personali_nascita_anno',
                                               label: 'Anno di nascita'},
  TUTORE_INFORMAZIONI_PERSONALI_NASCITA_NAZIONE: {nome: 'tutore_informazioni_personali_nascita_nazione',
                                                  label: 'Nazione di nascita'},
  TUTORE_INFORMAZIONI_PERSONALI_NASCITA_PROVINCIA: {nome: 'tutore_informazioni_personali_nascita_provincia',
                                                    label: 'Provincia di nascita'},
  TUTORE_INFORMAZIONI_PERSONALI_NASCITA_LOCALITA: {nome: 'tutore_informazioni_personali_nascita_localita',
                                                   label: 'Localit&agrave; di nascita'},
  TUTORE_INFORMAZIONI_PERSONALI_CODICE_FISCALE: {nome: 'tutore_informazioni_personali_codice_fiscale',
                                                 label: 'Codice fiscale'},
  TUTORE_INFORMAZIONI_PERSONALI_CITTADINANZA: {nome: 'tutore_informazioni_personali_cittadinanza',
                                               label: 'Cittadinanza'},
  TUTORE_RESIDENZA_NAZIONE: {nome: 'tutore_residenza_nazione',
                             label: 'Nazione di residenza'},
  TUTORE_RESIDENZA_PROVINCIA: {nome: 'tutore_residenza_provincia',
                               label: 'Provincia di nascita'},
  TUTORE_RESIDENZA_LOCALITA: {nome: 'tutore_residenza_localita',
                              label: 'Localit&agrave; di nascita'},
  TUTORE_RESIDENZA_CAP: {nome: 'tutore_residenza_cap',
                         label: 'CAP residenza'},
  TUTORE_RESIDENZA_RECAPITO_TIPO: {nome: 'tutore_residenza_recapito_tipo',
                                   label: 'Tipo indirizzo di residenza'},
  TUTORE_RESIDENZA_RECAPITO_INDIRIZZO: {nome: 'tutore_residenza_recapito_indirizzo',
                                        label: 'Indirizzo di residenza'},
  TUTORE_RESIDENZA_RECAPITO_NUMERO: {nome: 'tutore_residenza_recapito_numero',
                                     label: 'Numero indirizzo di residenza'},
  TUTORE_RESIDENZA_PRESSO: {nome: 'tutore_residenza_presso',
                            label: 'Presso (residenza)'},
  TUTORE_RESIDENZA_FISCALE_NAZIONE1: {nome: 'tutore_residenza_fiscale_nazione1',
                                      label: 'Nazione di residenza fiscale (scelta 1)'},
  TUTORE_RESIDENZA_FISCALE_CODICE_FISCALE_ESTERO1: {nome: 'tutore_residenza_fiscale_codice_fiscale_estero1',
                                                    label: 'Codice fiscale estero (scelta 1)'},
  TUTORE_RESIDENZA_FISCALE_NAZIONE2: {nome: 'tutore_residenza_fiscale_nazione2',
                                      label: 'Nazione di residenza fiscale (scelta 2)'},
  TUTORE_RESIDENZA_FISCALE_CODICE_FISCALE_ESTERO2: {nome: 'tutore_residenza_fiscale_codice_fiscale_estero2',
                                                    label: 'Codice fiscale estero (scelta 2)'},
  TUTORE_RESIDENZA_FISCALE_NAZIONE3: {nome: 'tutore_residenza_fiscale_nazione3',
                                      label: 'Nazione di residenza fiscale (scelta 3)'},
  TUTORE_RESIDENZA_FISCALE_CODICE_FISCALE_ESTERO3: {nome: 'tutore_residenza_fiscale_codice_fiscale_estero3',
                                                    label: 'Codice fiscale estero (scelta 3)'},
  TUTORE_DOCUMENTO_TIPO: {nome: 'tutore_documento_tipo',
                          label: 'Tipo documento'},
  TUTORE_DOCUMENTO_NUMERO: {nome: 'tutore_documento_numero',
                            label: 'Numero di documento'},
  TUTORE_DOCUMENTO_AUTORITA: {nome: 'tutore_documento_autorita',
                              label: 'Autorit&agrave; rilascio documento'},
  TUTORE_DOCUMENTO_NAZIONE: {nome: 'tutore_documento_nazione',
                             label: 'Nazione di rilascio documento'},
  TUTORE_DOCUMENTO_PROVINCIA: {nome: 'tutore_documento_provincia',
                               label: 'Provincia di rilascio documento'},
  TUTORE_DOCUMENTO_LOCALITA: {nome: 'tutore_documento_localita',
                              label: 'Localit&agrave; di rilascio documento'},
  TUTORE_DOCUMENTO_EMISSIONE: {nome: 'tutore_documento_emissione',
                               label: 'Data di emissione documento'},
  TUTORE_DOCUMENTO_EMISSIONE_GIORNO: {nome: 'tutore_documento_emissione_giorno',
                                      label: 'Giorno di emissione documento'},
  TUTORE_DOCUMENTO_EMISSIONE_MESE: {nome: 'tutore_documento_emissione_mese',
                                    label: 'Mese di emissione documento'},
  TUTORE_DOCUMENTO_EMISSIONE_ANNO: {nome: 'tutore_documento_emissione_anno',
                                    label: 'Anno di rilascio documento'},
  TUTORE_DOCUMENTO_SCADENZA: {nome: 'tutore_documento_scadenza',
                              label: 'Data di scadenza documento'},
  TUTORE_DOCUMENTO_SCADENZA_GIORNO: {nome: 'tutore_documento_scadenza_giorno',
                                     label: 'Giorno di scadenza documento'},
  TUTORE_DOCUMENTO_SCADENZA_MESE: {nome: 'tutore_documento_scadenza_mese',
                                   label: 'Mese di scadenza documento'},
  TUTORE_DOCUMENTO_SCADENZA_ANNO: {nome: 'tutore_documento_scadenza_anno',
                                   label: 'Anno di scadenza documento'},
  ELENCO_TIPOLOGIA_PROVINCIA: {nome: 'elenco_tipologia_provincia',
                               label: ''},
  ELENCO_PROVINCE_VALIDE_SEMPRE: {nome: 'elenco_province_valide_sempre',
                                  label: ''},
  ELENCO_PROVINCE_VALIDE_OGGI: {nome: 'elenco_province_valide_oggi',
                                label: ''},
  ELENCO_TIPOLOGIA_COMUNE: {nome: 'elenco_tipologia_comune',
                            label: ''},	
  ELENCO_COMUNI_VALIDI_SEMPRE: {nome: 'elenco_comuni_validi_oggi',
                                label: ''},
  ELENCO_COMUNI_VALIDI_OGGI: {nome: 'elenco_comuni_validi_oggi',
                              label: ''},
  ELENCO_COMUNI_DA_PROVINCIA: {nome: 'elenco_comuni_da_provincia',
                               label: ''}
};

const FIELD_LABELS = {
  tipologia_step: {label: ''},
  tipologia_richiesta_carta: {label: ''},
  consenso_informativa: {label: 'Ho preso visione dell\'informativa pre-contrattuale'},
  consenso_trattamento_dati_personali: {label: 'Presto il consenso'},
  consenso_c3: {label: ''},
  consenso_c4: {label: ''},
  consenso_c5: {label: ''},
  titolare_informazioni_personali_nome: {label: 'Nome'},
  titolare_informazioni_personali_cognome: {label: 'Cognome'},
  titolare_informazioni_personali_sesso: {label: 'Sesso'},
  titolare_informazioni_personali_nascita: {label: 'Data di nascita'},
  titolare_informazioni_personali_nascita_giorno: {label: 'Giorno di nascita'},
  titolare_informazioni_personali_nascita_mese: {label: 'Mese di nascita'},
  titolare_informazioni_personali_nascita_anno: {label: 'Anno di nascita'},
  titolare_informazioni_personali_nascita_nazione: {label: 'Nazione di nascita'},
  titolare_informazioni_personali_nascita_provincia: {label: 'Provincia di nascita'},
  titolare_informazioni_personali_nascita_localita: {label: 'Località di nascita'},
  titolare_informazioni_personali_codice_fiscale: {label: 'Codice fiscale'},
  titolare_informazioni_personali_cittadinanza: {label: 'Cittadinanza'},
  titolare_contatti_email: {label: 'Email'},
  titolare_contatti_ripeti_email: {label: 'Ripeti email'},
  titolare_contatti_telefono_cellulare: {label: 'Telefono cellulare'},
  titolare_contatti_telefono_abitazione: {label: 'Telefono abitazione'},
  titolare_contatti_telefono_ufficio: {label: 'Telefono ufficio'},
  titolare_residenza_nazione: {label: 'Nazione di residenza'},
  titolare_residenza_provincia: {label: 'Provincia di residenza'},
  titolare_residenza_localita: {label: 'Località di residenza'},
  titolare_residenza_cap: {label: 'C.A.P. residenza'},
  titolare_residenza_recapito_tipo: {label: 'Indirizzo di residenza'},
  titolare_residenza_recapito_indirizzo: {label: 'Indirizzo di residenza'},
  titolare_residenza_recapito_numero: {label: 'Numero indirizzo di residenza'},
  titolare_residenza_presso: {label: 'Presso (residenza)'},
  titolare_residenza_fiscale_nazione1: {label: 'Residenza fiscale'},
  titolare_residenza_fiscale_codice_fiscale_estero1: {label: 'Codice fiscale estero (scelta 1)'},
  titolare_residenza_fiscale_nazione2: {label: 'Residenza fiscale (scelta 2)'},
  titolare_residenza_fiscale_codice_fiscale_estero2: {label: 'Codice fiscale estero (scelta 2)'},
  titolare_residenza_fiscale_nazione3: {label: 'Residenza fiscale (scelta 3)'},
  titolare_residenza_fiscale_codice_fiscale_estero3: {label: 'Codice fiscale estero (scelta 3)'},
  titolare_domicilio_nazione: {label: 'Nazione di domicilio'},
  titolare_domicilio_provincia: {label: 'Provincia di domicilio'},
  titolare_domicilio_localita: {label: 'Località di domicilio'},
  titolare_domicilio_cap: {label: 'C.A.P. domicilio'},
  titolare_domicilio_recapito_tipo: {label: 'Tipo indirizzo di domicilio'},
  titolare_domicilio_recapito_indirizzo: {label: 'Indirizzo di domicilio'},
  titolare_domicilio_recapito_numero: {label: 'Numero indirizzo di domicilio'},
  titolare_domicilio_presso: {label: 'Presso (domicilio)'},
  titolare_documento_tipo: {label: 'Tipo documento'},
  titolare_documento_numero: {label: 'Numero di documento'},
  titolare_documento_autorita: {label: 'Autorità di rilascio documento'},
  titolare_documento_nazione: {label: 'Nazione di rilascio documento'},
  titolare_documento_provincia: {label: 'Provincia di rilascio documento'},
  titolare_documento_localita: {label: 'Località di rilascio documento'},
  titolare_documento_emissione: {label: 'Data di emissione/ultimo rinnovo documento'},
  titolare_documento_emissione_giorno: {label: 'Giorno di emissione/ultimo rinnovo documento'},
  titolare_documento_emissione_mese: {label: 'Mese di emissione/ultimo rinnovo documento'},
  titolare_documento_emissione_anno: {label: 'Anno di emissione/ultimo rinnovo documento'},
  titolare_documento_scadenza: {label: 'Data di scadenza documento'},
  titolare_documento_scadenza_giorno: {label: 'Giorno di scadenza documento'},
  titolare_documento_scadenza_mese: {label: 'Mese di scadenza documento'},
  titolare_documento_scadenza_anno: {label: 'Anno di scadenza documento'},
  titolare_dati_professionali_abi: {label: 'Abi'},
  titolare_dati_professionali_cab: {label: 'Cab'},
  titolare_dati_professionali_agenzia: {label: 'Agenzia'},
  titolare_dati_professionali_cin: {label: 'Cin'},
  titolare_dati_professionali_professione: {label: 'Professione'},
  titolare_dati_professionali_fascia_reddito: {label: 'Reddito'},
  titolare_dati_professionali_tipo_attivita_economica: {label: 'Tipo attività economica'},
  titolare_dati_professionali_datore_lavoro: {label: 'Datore di lavoro'},
  titolare_dati_professionali_provincia: {label: 'Provincia'},
  titolare_dati_professionali_nazione: {label: 'Nazione'},
  titolare_dati_professionali_fondi: {label: 'Fondi'},
  titolare_dati_professionali_fondi_altra_provenienza: {label: 'Altra provenienza'},
  titolare_dati_professionali_finalita_rapporto: {label: 'Finalità del rapporto'},
  titolare_dati_professionali_esposizione_politica: {label: 'Esposizione politica'},
  tutore_informazioni_personali_nome: {label: 'Nome tutore'},
  tutore_informazioni_personali_cognome: {label: 'Cognome tutore'},
  tutore_informazioni_personali_sesso: {label: 'Sesso tutore'},
  tutore_informazioni_personali_nascita: {label: 'Data di nascita tutore'},
  tutore_informazioni_personali_nascita_giorno: {label: 'Giorno di nascita tutore'},
  tutore_informazioni_personali_nascita_mese: {label: 'Mese di nascita tutore'},
  tutore_informazioni_personali_nascita_anno: {label: 'Anno di nascita tutore'},
  tutore_informazioni_personali_nascita_nazione: {label: 'Nazione di nascita tutore'},
  tutore_informazioni_personali_nascita_provincia: {label: 'Provincia di nascita tutore'},
  tutore_informazioni_personali_nascita_localita: {label: 'Località di nascita tutore'},
  tutore_informazioni_personali_codice_fiscale: {label: 'Codice fiscale tutore'},
  tutore_informazioni_personali_cittadinanza: {label: 'Cittadinanza tutore'},
  tutore_residenza_nazione: {label: 'Nazione di residenza tutore'},
  tutore_residenza_provincia: {label: 'Provincia di nascita tutore'},
  tutore_residenza_localita: {label: 'Località di nascita tutore'},
  tutore_residenza_cap: {label: 'C.A.P. residenza tutore'},
  tutore_residenza_recapito_tipo: {label: 'Indirizzo di residenza tutore'},
  tutore_residenza_recapito_indirizzo: {label: 'Indirizzo di residenza tutore'},
  tutore_residenza_recapito_numero: {label: 'Numero indirizzo di residenza tutore'},
  tutore_residenza_presso: {label: 'Presso (residenza tutore)'},
  tutore_residenza_fiscale_nazione1: {label: 'Residenza fiscale tutore'},
  tutore_residenza_fiscale_codice_fiscale_estero1: {label: 'Codice fiscale estero tutore (scelta 1)'},
  tutore_residenza_fiscale_nazione2: {label: 'Residenza fiscale tutore (scelta 2)'},
  tutore_residenza_fiscale_codice_fiscale_estero2: {label: 'Codice fiscale estero tutore (scelta 2)'},
  tutore_residenza_fiscale_nazione3: {label: 'Residenza fiscale tutore (scelta 3)'},
  tutore_residenza_fiscale_codice_fiscale_estero3: {label: 'Codice fiscale estero tutore (scelta 3)'},
  tutore_documento_tipo: {label: 'Tipo documento tutore'},
  tutore_documento_numero: {label: 'Numero di documento tutore'},
  tutore_documento_autorita: {label: 'Autorità di rilascio documento tutore'},
  tutore_documento_nazione: {label: 'Nazione di rilascio documento tutore'},
  tutore_documento_provincia: {label: 'Provincia di rilascio documento tutore'},
  tutore_documento_localita: {label: 'Località di rilascio documento tutore'},
  tutore_documento_emissione: {label: 'Data di emissione/ultimo rinnovo documento tutore'},
  tutore_documento_emissione_giorno: {label: 'Giorno di emissione/ultimo rinnovo documento tutore'},
  tutore_documento_emissione_mese: {label: 'Mese di emissione/ultimo rinnovo documento tutore'},
  tutore_documento_emissione_anno: {label: 'Anno di emissione/ultimo rinnovo documento tutore'},
  tutore_documento_scadenza: {label: 'Data di scadenza documento tutore'},
  tutore_documento_scadenza_giorno: {label: 'Giorno di scadenza documento tutore'},
  tutore_documento_scadenza_mese: {label: 'Mese di scadenza documento tutore'},
  tutore_documento_scadenza_anno: {label: 'Anno di scadenza documento tutore'},
  elenco_tipologia_provincia: {label: ''},
  elenco_province_valide_sempre: {label: ''},
  elenco_province_valide_oggi: {label: ''},
  elenco_tipologia_comune: {label: ''},	
  elenco_comuni_validi_sempre: {label: ''},
  elenco_comuni_validi_oggi: {label: ''},
  elenco_comuni_da_provincia: {label: ''}
};

const FORM_TO_HTTP = {
  step: 'tipologia_step',
  tipologiaRichiestaCarta: 'tipologia_carta',
  consensi: {
    informativa: 'consenso_informativa',
    trattamentoDatiPersonali: 'consenso_trattamento_dati_personali',
    C3: 'consenso_c3',
    C4: 'consenso_c4',
    C5: 'consenso_c5',
  },
  titolare: {
    informazioniPersonali: {
      nome: 'titolare_informazioni_personali_nome',
      cognome: 'titolare_informazioni_personali_cognome',
      sesso: 'titolare_informazioni_personali_sesso',
      dataDiNascita: {giorno: 'titolare_informazioni_personali_nascita_giorno',
                      mese: 'titolare_informazioni_personali_nascita_mese',
                      anno: 'titolare_informazioni_personali_nascita_anno'},
      nazioneDiNascita: 'titolare_informazioni_personali_nascita_nazione',
      provinciaDiNascita: 'titolare_informazioni_personali_nascita_provincia',
      localitaDiNascita: 'titolare_informazioni_personali_nascita_localita',
      codiceFiscale: 'titolare_informazioni_personali_codice_fiscale',
      cittadinanza: 'titolare_informazioni_personali_cittadinanza'
    },
    contatti: {
      email: 'titolare_contatti_email',
      ripetiEmail: 'titolare_contatti_ripeti_email',
      telefonoCellulare: 'titolare_contatti_telefono_cellulare',
      telefonoAbitazione: 'titolare_contatti_telefono_abitazione',
      telefonoUfficio: 'titolare_contatti_telefono_ufficio'
    },
    residenza: {
      nazione: 'titolare_residenza_nazione',
      provincia: 'titolare_residenza_provincia',
      localita: 'titolare_residenza_localita',
      cap: 'titolare_residenza_cap',
      recapito: {tipo: 'titolare_residenza_recapito_tipo',
                 indirizzo: 'titolare_residenza_recapito_indirizzo',
                 numero: 'titolare_residenza_recapito_numero'},
      presso: 'titolare_residenza_presso'
    },
    residenzaFiscale: {
      nazione1: 'titolare_residenza_fiscale_nazione1',
      codiceFiscaleEstero1: 'titolare_residenza_fiscale_codice_fiscale_estero1',
      nazione2: 'titolare_residenza_fiscale_nazione2',
      codiceFiscaleEstero2: 'titolare_residenza_fiscale_codice_fiscale_estero2',
      nazione3: 'titolare_residenza_fiscale_nazione3',
      codiceFiscaleEstero3: 'titolare_residenza_fiscale_codice_fiscale_estero3'
    },
    domicilio: {
      nazione: 'titolare_domicilio_nazione',
      provincia: 'titolare_domicilio_provincia',
      localita: 'titolare_domicilio_localita',
      cap: 'titolare_domicilio_cap',
      recapito: {tipo: 'titolare_domicilio_recapito_tipo',
                 indirizzo: 'titolare_domicilio_recapito_indirizzo',
                 numero: 'titolare_domicilio_recapito_numero'},
      presso: 'titolare_domicilio_presso'
    },
    documento: {
      tipo: 'titolare_documento_tipo',
      numero: 'titolare_documento_numero',
      autorita: 'titolare_documento_autorita',
      nazione: 'titolare_documento_nazione',
      provincia: 'titolare_documento_provincia',
      localita: 'titolare_documento_localita',
      dataDiEmissione: {giorno: 'titolare_documento_emissione_giorno',
                        mese: 'titolare_documento_emissione_mese',
                        anno: 'titolare_documento_emissione_anno'},
      dataDiScadenza: {giorno: 'titolare_documento_scadenza_giorno',
                       mese: 'titolare_documento_scadenza_mese',
                       anno: 'titolare_documento_scadenza_anno'}
    },
    datiProfessionali: {
      abi: 'titolare_dati_professionali_abi',
      cab: 'titolare_dati_professionali_cab',
      agenzia: 'titolare_dati_professionali_agenzia',
      cin: 'titolare_dati_professionali_cin',
      professione: 'titolare_dati_professionali_professione',
      fasciaReddito: 'titolare_dati_professionali_fascia_reddito',
      tipoAttivitaEconomica: 'titolare_dati_professionali_tipo_attivita_economica',
      datoreLavoro: 'titolare_dati_professionali_datore_lavoro',
      provincia: 'titolare_dati_professionali_provincia',
      nazione: 'titolare_dati_professionali_nazione',
      fondi: {provenienze: 'titolare_dati_professionali_fondi',
              altraProvenienza: 'titolare_dati_professionali_fondi_altra_provenienza'},
      finalitaRapporto: 'titolare_dati_professionali_finalita_rapporto',
      esposizionePolitica: 'titolare_dati_professionali_esposizione_politica'
    }
  },
  tutore: {
    informazioniPersonali: {
      nome: 'tutore_informazioni_personali_nome',
      cognome: 'tutore_informazioni_personali_cognome',
      sesso: 'tutore_informazioni_personali_sesso',
      dataDiNascita: {giorno: 'tutore_informazioni_personali_nascita_giorno',
                      mese: 'tutore_informazioni_personali_nascita_mese',
                      anno: 'tutore_informazioni_personali_nascita_anno'},
      nazioneDiNascita: 'tutore_informazioni_personali_nascita_nazione',
      provinciaDiNascita: 'tutore_informazioni_personali_nascita_provincia',
      localitaDiNascita: 'tutore_informazioni_personali_nascita_localita',
      codiceFiscale: 'tutore_informazioni_personali_codice_fiscale',
      cittadinanza: 'tutore_informazioni_personali_cittadinanza'
    },
    residenza: {
      nazione: 'tutore_residenza_nazione',
      provincia: 'tutore_residenza_provincia',
      localita: 'tutore_residenza_localita',
      cap: 'tutore_residenza_cap',
      recapito: {tipo: 'tutore_residenza_recapito_tipo',
                 indirizzo: 'tutore_residenza_recapito_indirizzo',
                 numero: 'tutore_residenza_recapito_numero'},
      presso: 'tutore_residenza_presso'
    },
    residenzaFiscale: {
      nazione1: 'tutore_residenza_fiscale_nazione1',
      codiceFiscaleEstero1: 'tutore_residenza_fiscale_codice_fiscale_estero1',
      nazione2: 'tutore_residenza_fiscale_nazione2',
      codiceFiscaleEstero2: 'tutore_residenza_fiscale_codice_fiscale_estero2',
      nazione3: 'tutore_residenza_fiscale_nazione3',
      codiceFiscaleEstero3: 'tutore_residenza_fiscale_codice_fiscale_estero3'
    },
    documento: {
      tipo: 'tutore_documento_tipo',
      numero: 'tutore_documento_numero',
      autorita: 'tutore_documento_autorita',
      nazione: 'tutore_documento_nazione',
      provincia: 'tutore_documento_provincia',
      localita: 'tutore_documento_localita',
      dataDiEmissione: {giorno: 'tutore_documento_emissione_giorno',
                        mese: 'tutore_documento_emissione_mese',
                        anno: 'tutore_documento_emissione_anno'},
      dataDiScadenza: {giorno: 'tutore_documento_scadenza_giorno',
                       mese: 'tutore_documento_scadenza_mese',
                       anno: 'tutore_documento_scadenza_anno'}
    },
  }
};

class Step extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className={this.props.labelClasses}>
        <span className='stepLabelNumber'>{this.props.labelNumber}</span>
        <span className='stepLabelText'>{this.props.labelText}</span>
        <span className={this.props.arrowClasses}></span>
      </li>
    );
  }
}

class StepBar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    
    const current = this.props.activeStep.stepbarNumber;
    const steplist = STEPBAR_LABELS.map((label, index) => {
      var labelClasses = (label.number < current) ? 'stepGeneric stepDone' :
                    (label.number === current) ? 'stepGeneric stepActive' : 'stepGeneric stepDisabled';
      var arrowClasses = (label.number < current) ? 'stepArrow stepArrowDone' :
                    (label.number === current) ? 'stepArrow stepArrowActive' : 'stepArrow stepArrowDisabled';
      labelClasses = labelClasses.concat(' stepNumber' + label.number);
      return (
        <Step key={index} labelNumber={label.number} labelText={label.text} labelClasses={labelClasses} arrowClasses={arrowClasses} />
      );
    });

    return (
      <div id='gadgetStepbar'>
        <ul>
          {steplist}
        </ul>
      </div>
    );
  }
}

class Fine extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h2>Vendita</h2>
        <p>
          Complimenti per aver scelto una CartaSi.
          <br/>
          La carta &egrave; valida da subito per ricaricarla&nbsp;
          <Link id='linkclicca' to='ricarica'>clicca qui</Link>
        </p>
      </div>
    );
  }
}

class DownloadContractButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button className='downloadButton' onClick={this.props.onClick}>
        Scarica e stampa contratto
      </button>
    );
  }
}

class ProseguiButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button className='navigateProseguiButton' onClick={this.props.onClick}>
        Prosegui
      </button>
    );
  }
}

class IndietroButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button className='navigateIndietroButton' onClick={this.props.onClick}>
        Indietro
      </button>
    );
  }
}

function stringify(object, parameters) {
  var string = '';
  for (var key in object) {
    if (string != '') {
      string += '&';
    }
    if (object[key].keys !== undefined) {
      var parameter = parameters[key];
      if (parameter !== undefined) {
        if (object[key].size === 0) {
          string += parameter + '=';
        } else {
          for (let item of object[key]) {
            string += parameter + '=' + item + '&';
          }
          string = string.substring(0,string.lastIndexOf('&'));
        }
      } else {
        string = string.substring(0,string.lastIndexOf('&'));
      }
    } else if (object[key] !== null && typeof object[key] === 'object') {
      string += stringify(object[key], parameters[key]);
    } else {
      var parameter = parameters[key];
      if (parameter !== undefined) {
        string += parameter + '=' + object[key];
      } else {
        string = string.substring(0,string.lastIndexOf('&'));
      }
    }
  }
  return string;
}

function serialize(data) {
  var parameter = FORM_TO_HTTP.step;
  var serialized = parameter + '=' + data.steps[data.currentStepNumber-1];
 
  parameter = FORM_TO_HTTP.tipologiaRichiestaCarta;
  serialized += '&' + parameter + '=' + data.tipologiaCarta;

  switch (data.steps[data.currentStepNumber-1]) {
    case 'INFORMATIVA':
      parameter = FORM_TO_HTTP['consensi'].informativa;
      serialized += '&' + parameter + '=' + data.consensi.informativa;
      break;
    case 'DATI_PERSONALI':
      serialized += '&' + stringify(
          data.titolare.informazioniPersonali,
          FORM_TO_HTTP['titolare']['informazioniPersonali']);
      serialized += '&' + stringify(
          data.titolare.contatti,
          FORM_TO_HTTP['titolare']['contatti']);
      serialized += '&' + stringify(
          data.titolare.residenza,
          FORM_TO_HTTP['titolare']['residenza']);
      serialized += '&' + stringify(
          data.titolare.residenzaFiscale,
          FORM_TO_HTTP['titolare']['residenzaFiscale']);
      serialized += '&' + stringify(
          data.titolare.domicilio,
          FORM_TO_HTTP['titolare']['domicilio']);
      serialized += '&' + stringify(
          data.titolare.documento,
          FORM_TO_HTTP['titolare']['documento']);

      if (data.tipologiaCarta === TIPOLOGIE_CARTA['CONTRATTUALIZZATA_MINORE']) {
        serialized += '&' + stringify(
            data.tutore.informazioniPersonali,
            FORM_TO_HTTP['tutore']['informazioniPersonali']);
        serialized += '&' + stringify(
            data.tutore.residenza,
            FORM_TO_HTTP['tutore']['residenza']);
        serialized += '&' + stringify(
            data.tutore.residenzaFiscale,
            FORM_TO_HTTP['tutore']['residenzaFiscale']);
        serialized +='&' + stringify(
            data.tutore.documento,
            FORM_TO_HTTP['tutore']['documento']);
      }
      break;
    case 'DATI_PROFESSIONALI':
      serialized += '&' + stringify(
          data.titolare.datiProfessionali,
          FORM_TO_HTTP['titolare']['datiProfessionali']);
      break;
    case 'RIEPILOGO':
      parameter = FORM_TO_HTTP['consensi'].trattamentoDatiPersonali;
      serialized += '&' + parameter + '=' + data.consensi.trattamentoDatiPersonali;

      parameter = FORM_TO_HTTP['consensi'].C3;
      serialized += '&' + parameter + '=' + data.consensi.C3;

      parameter = FORM_TO_HTTP['consensi'].C4;
      serialized += '&' + parameter + '=' + data.consensi.C4;

      parameter = FORM_TO_HTTP['consensi'].C5;
      serialized += '&' + parameter + '=' + data.consensi.C5;
      break;
    case 'FINALIZZAZIONE':
      break;
    default:
      console.log('step default');
  }
  console.log('STRINGA TO SERVLET ' + serialized);
  return serialized;
}

class RichiestaWoRouter extends React.Component {
  constructor(props) {
    super(props);
    const { match, location, history } = this.props;
    this.state = {
      currentStepNumber: 1,
      steps: STEP_CARTA[this.props.tipoCarta],
      tipologiaCarta: this.props.tipoCarta,
      consensi: {
        informativa: false,
        trattamentoDatiPersonali: false,
        C3: false,
        C4: false,
        C5: false
      },
      titolare: {
        informazioniPersonali: {
          nome: '',
          cognome: '',
          sesso: '',
          dataDiNascita: {giorno: '', mese: '', anno: ''},
          nazioneDiNascita: '',
          provinciaDiNascita: '',
          localitaDiNascita: '',
          codiceFiscale: '',
          cittadinanza: ''
        },
        contatti: {
          email: '',
          ripetiEmail: '',
          telefonoCellulare: '',
          telefonoAbitazione: '',
          telefonoUfficio: ''
        },
        residenza: {
          nazione: 'ITALIA',
          provincia: '',
          localita: '',
          cap: '',
          recapito: {tipo: 'VIA', indirizzo: '', numero: ''},
          presso: ''
        },
        residenzaFiscale: {
          nazione1: '',
          codiceFiscaleEstero1: '',
          nazione2: '',
          codiceFiscaleEstero2: '',
          nazione3: '',
          codiceFiscaleEstero3: ''
        },
        domicilio: {
          nazione: 'ITALIA',
          provincia: '',
          localita: '',
          cap: '',
          recapito: {tipo: '', indirizzo: '', numero: ''},
          presso: ''
        },
        documento: {
          tipo: '01',
          numero: '',
          autorita: 'COMUNE',
          nazione: '',
          provincia: '',
          localita: '',
          dataDiEmissione: {giorno: '', mese: '', anno: ''},
          dataDiScadenza: {giorno: '', mese: '', anno: ''}
        },
        datiProfessionali: {
          abi: '',
          cab: '',
          agenzia: '',
          cin: '',
          professione: '',
          fasciaReddito: '',
          tipoAttivitaEconomica: '',
          datoreLavoro: '',
          provincia: '',
          nazione: '',
          fondi: {provenienze: new Set(),
                  labels: new Set(),
                  altraProvenienza: ''},
          finalitaRapporto: new Set(),
          esposizionePolitica: ''
        },
        validazione: {
          codiceFiscale: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''},
          dataDiNascita: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''},
          email: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''},
          dataEmissioneDocumento: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''},
          dataScadenzaDocumento: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''}
        }
      },
      tutore: {
        informazioniPersonali: {
          nome: '',
          cognome: '',
          sesso: '',
          dataDiNascita: {giorno: '', mese: '', anno: ''},
          nazioneDiNascita: '',
          provinciaDiNascita: '',
          localitaDiNascita: '',
          codiceFiscale: '',
          cittadinanza: ''
        },
        residenza: {
          nazione: 'ITALIA',
          provincia: '',
          localita: '',
          cap: '',
          recapito: {tipo: 'VIA', indirizzo: '', numero: ''},
          presso: ''
        },
        residenzaFiscale: {
          nazione1: '',
          codiceFiscaleEstero1: '',
          nazione2: '',
          codiceFiscaleEstero2: '',
          nazione3: '',
          codiceFiscaleEstero3: ''
        },
        documento: {
          tipo: '01',
          numero: '',
          autorita: 'COMUNE',
          nazione: '',
          provincia: '',
          localita: '',
          dataDiEmissione: {giorno: '', mese: '', anno: ''},
          dataDiScadenza: {giorno: '', mese: '', anno: ''}
        },
        validazione: {
          codiceFiscale: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''},
          dataDiNascita: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''},
          dataEmissioneDocumento: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''},
          dataScadenzaDocumento: {
            status: FIELD_STATUS['TO_CHECK'],
            message: ''}
        }
      },
      errori: []
    };
    this.handleProseguiClick = this.handleProseguiClick.bind(this);
    this.handleIndietroClick = this.handleIndietroClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDownloadContractClick = this.handleDownloadContractClick.bind(this);
    this.handleModificaClick = this.handleModificaClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFocusOut = this.handleFocusOut.bind(this);
  }

  handleFocusOut(event) {
    if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].dataDiNascita.giorno ||
        event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].dataDiNascita.mese ||
        event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].dataDiNascita.anno) {
      if (this.state.titolare.informazioniPersonali.dataDiNascita.giorno != '' &&
          this.state.titolare.informazioniPersonali.dataDiNascita.mese != '' &&
          this.state.titolare.informazioniPersonali.dataDiNascita.anno != '') {
        var errMessage = '';
        if (this.props.tipoCarta === TIPOLOGIE_CARTA['CONTRATTUALIZZATA'] ||
            this.props.tipoCarta === TIPOLOGIE_CARTA['NON_CONTRATTUALIZZATA']) {
          errMessage = validaMaggiorenne(parseInt(this.state.titolare.informazioniPersonali.dataDiNascita.giorno),
                                         parseInt(this.state.titolare.informazioniPersonali.dataDiNascita.mese),
                                         parseInt(this.state.titolare.informazioniPersonali.dataDiNascita.anno));
        } else if (this.props.tipoCarta === TIPOLOGIE_CARTA['CONTRATTUALIZZATA_MINORE']) {
          {/*
            do nothing
          */}
        } else {  
          {/*
          do nothing
        */}
        }

        if (errMessage != '') {
          this.setState({
            titolare: Object.assign(
              this.state.titolare, {
              validazione: Object.assign(
                this.state.titolare.validazione, {
                dataDiNascita: {
                  status: FIELD_STATUS['KO'],
                  message: errMessage}})})});
        } else {
          this.setState({
            titolare: Object.assign(
              this.state.titolare, {
              validazione: Object.assign(
                this.state.titolare.validazione, {
                dataDiNascita: {
                  status: FIELD_STATUS['OK'],
                  message: ''}})})});
        }
      } else {
        
        this.setState({
          titolare: Object.assign(
            this.state.titolare, {
            validazione: Object.assign(
              this.state.titolare.validazione, {
              dataDiNascita: {
                status: FIELD_STATUS['TO_CHECK'],
                message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].codiceFiscale) {
      var errMessage = validaCodiceFiscale(this.state.titolare.informazioniPersonali.codiceFiscale);
      if (errMessage != '') {
        this.setState({
          titolare: Object.assign(
            this.state.titolare, {
            validazione: Object.assign(
              this.state.titolare.validazione, {
              codiceFiscale: {
                status: FIELD_STATUS['KO'],
                message: errMessage}})})});
      } else {
        this.setState({
          titolare: Object.assign(
            this.state.titolare, {
            validazione: Object.assign(
              this.state.titolare.validazione, {
              codiceFiscale: {
                status: FIELD_STATUS['OK'],
                message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['CONTATTI'].email ||
               event.target.id === IDS_DATI_PERSONALI['TITOLARE']['CONTATTI'].ripetiEmail) {
      if (this.state.titolare.contatti.email != '' &&
          this.state.titolare.contatti.ripetiEmail != '') {
        var errMessage = validaEmail(this.state.titolare.contatti.email, this.state.titolare.contatti.ripetiEmail);
        if (errMessage != '') {
          this.setState({
            titolare: Object.assign(
              this.state.titolare, {
              validazione: Object.assign(
                this.state.titolare.validazione, {
                email: {
                  status: FIELD_STATUS['KO'],
                  message: errMessage}})})});
        } else {
          this.setState({
            titolare: Object.assign(
              this.state.titolare, {
              validazione: Object.assign(
                this.state.titolare.validazione, {
                email: {
                  status: FIELD_STATUS['OK'],
                  message: ''}})})});
        }
      } else {
        this.setState({
          titolare: Object.assign(
            this.state.titolare, {
            validazione: Object.assign(
              this.state.titolare.validazione, {
              email: {
                status: FIELD_STATUS['TO_CHECK'],
                message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiEmissione.giorno ||
               event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiEmissione.mese ||
               event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiEmissione.anno) {
      if (this.state.titolare.documento.dataDiEmissione.giorno != '' &&
          this.state.titolare.documento.dataDiEmissione.mese != '' &&
          this.state.titolare.documento.dataDiEmissione.anno != '') {
        var errMessage = validaDataEmissioneDocumento(parseInt(this.state.titolare.documento.dataDiEmissione.giorno),
                                                      parseInt(this.state.titolare.documento.dataDiEmissione.mese),
                                                      parseInt(this.state.titolare.documento.dataDiEmissione.anno));
        if (errMessage != '') {
          this.setState({
            titolare: Object.assign(
              this.state.titolare, {
              validazione: Object.assign(
                this.state.titolare.validazione, {
                dataEmissioneDocumento: {
                  status: FIELD_STATUS['KO'],
                  message: errMessage}})})});
        } else {
          this.setState({
            titolare: Object.assign(
              this.state.titolare, {
              validazione: Object.assign(
                this.state.titolare.validazione, {
                dataEmissioneDocumento: {
                  status: FIELD_STATUS['OK'],
                  message: ''}})})});
        }
      } else {
        this.setState({
          titolare: Object.assign(
            this.state.titolare, {
            validazione: Object.assign(
              this.state.titolare.validazione, {
              dataEmissioneDocumento: {
                status: FIELD_STATUS['TO_CHECK'],
                message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiScadenza.giorno ||
               event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiScadenza.mese ||
               event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiScadenza.anno) {
      if (this.state.titolare.documento.dataDiScadenza.giorno != '' &&
          this.state.titolare.documento.dataDiScadenza.mese != '' &&
          this.state.titolare.documento.dataDiScadenza.anno != '') {
        var errMessage = validaDataScadenzaDocumento(parseInt(this.state.titolare.documento.dataDiScadenza.giorno),
                                                     parseInt(this.state.titolare.documento.dataDiScadenza.mese),
                                                     parseInt(this.state.titolare.documento.dataDiScadenza.anno));
        if (errMessage != '') {
          this.setState({
          titolare: Object.assign(
            this.state.titolare, {
            validazione: Object.assign(
              this.state.titolare.validazione, {
              dataScadenzaDocumento: {
                status: FIELD_STATUS['KO'],
                message: errMessage}})})});
        } else {
          this.setState({
          titolare: Object.assign(
            this.state.titolare, {
            validazione: Object.assign(
              this.state.titolare.validazione, {
              dataScadenzaDocumento: {
                status: FIELD_STATUS['OK'],
                message: ''}})})});
        }
      } else {
      this.setState({
      titolare: Object.assign(
      this.state.titolare, {
      validazione: Object.assign(
        this.state.titolare.validazione, {
        dataScadenzaDocumento: {
          status: FIELD_STATUS['TO_CHECK'],
          message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].dataDiNascita.giorno ||
               event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].dataDiNascita.mese ||
               event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].dataDiNascita.anno) {
      if (this.state.tutore.informazioniPersonali.dataDiNascita.giorno != '' &&
          this.state.tutore.informazioniPersonali.dataDiNascita.mese != '' &&
          this.state.tutore.informazioniPersonali.dataDiNascita.anno != '') {
        var errMessage = validaMaggiorenne(parseInt(this.state.tutore.informazioniPersonali.dataDiNascita.giorno),
                                          parseInt(this.state.tutore.informazioniPersonali.dataDiNascita.mese),
                                          parseInt(this.state.tutore.informazioniPersonali.dataDiNascita.anno));
        if (errMessage != '') {
          this.setState({
            tutore: Object.assign(
              this.state.tutore, {
              validazione: Object.assign(
                this.state.tutore.validazione, {
                dataDiNascita: {
                  status: FIELD_STATUS['KO'],
                  message: errMessage}})})});
        } else {
          this.setState({
            tutore: Object.assign(
              this.state.tutore, {
              validazione: Object.assign(
                this.state.tutore.validazione, {
                dataDiNascita: {
                  status: FIELD_STATUS['OK'],
                  message: ''}})})});
        }
      } else {
        this.setState({
          tutore: Object.assign(
            this.state.tutore, {
            validazione: Object.assign(
              this.state.tutore.validazione, {
              dataDiNascita: {
                status: FIELD_STATUS['TO_CHECK'],
                message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].codiceFiscale) {
      var errMessage = validaCodiceFiscale(this.state.tutore.informazioniPersonali.codiceFiscale);
      if (errMessage != '') {
        this.setState({
          tutore: Object.assign(
            this.state.tutore, {
            validazione: Object.assign(
              this.state.tutore.validazione, {
              codiceFiscale: {
                status: FIELD_STATUS['KO'],
                message: errMessage}})})});
      } else {
        this.setState({
          tutore: Object.assign(
            this.state.tutore, {
            validazione: Object.assign(
              this.state.tutore.validazione, {
              codiceFiscale: {
                status: FIELD_STATUS['OK'],
                message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiEmissione.giorno ||
              event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiEmissione.mese ||
              event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiEmissione.anno) {
      if (this.state.tutore.documento.dataDiEmissione.giorno != '' &&
          this.state.tutore.documento.dataDiEmissione.mese != '' &&
          this.state.tutore.documento.dataDiEmissione.anno != '') {
        var errMessage = validaDataEmissioneDocumento(parseInt(this.state.tutore.documento.dataDiEmissione.giorno),
                                                      parseInt(this.state.tutore.documento.dataDiEmissione.mese),
                                                      parseInt(this.state.tutore.documento.dataDiEmissione.anno));
        if (errMessage != '') {
          this.setState({
            tutore: Object.assign(
              this.state.tutore, {
              validazione: Object.assign(
                this.state.tutore.validazione, {
                dataEmissioneDocumento: {
                  status: FIELD_STATUS['KO'],
                  message: errMessage}})})});
        } else {
          this.setState({
            tutore: Object.assign(
              this.state.tutore, {
              validazione: Object.assign(
                this.state.tutore.validazione, {
                dataEmissioneDocumento: {
                  status: FIELD_STATUS['OK'],
                  message: ''}})})});
        }
      } else {
        this.setState({
          tutore: Object.assign(
            this.state.tutore, {
            validazione: Object.assign(
              this.state.tutore.validazione, {
              dataEmissioneDocumento: {
                status: FIELD_STATUS['TO_CHECK'],
                message: ''}})})});
      }
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiScadenza.giorno ||
              event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiScadenza.mese ||
              event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiScadenza.anno) {
      if (this.state.tutore.documento.dataDiScadenza.giorno != '' &&
          this.state.tutore.documento.dataDiScadenza.mese != '' &&
          this.state.tutore.documento.dataDiScadenza.anno != '') {
        var errMessage = validaDataScadenzaDocumento(parseInt(this.state.tutore.documento.dataDiScadenza.giorno),
                                                    parseInt(this.state.tutore.documento.dataDiScadenza.mese),
                                                    parseInt(this.state.tutore.documento.dataDiScadenza.anno));
        if (errMessage != '') {
          this.setState({
          tutore: Object.assign(
            this.state.tutore, {
            validazione: Object.assign(
              this.state.tutore.validazione, {
              dataScadenzaDocumento: {
                status: FIELD_STATUS['KO'],
                message: errMessage}})})});
        } else {
          this.setState({
          tutore: Object.assign(
            this.state.tutore, {
            validazione: Object.assign(
              this.state.tutore.validazione, {
              dataScadenzaDocumento: {
                status: FIELD_STATUS['OK'],
                message: ''}})})});
        }
      } else {
      this.setState({
      tutore: Object.assign(
      this.state.tutore, {
      validazione: Object.assign(
        this.state.tutore.validazione, {
        dataScadenzaDocumento: {
          status: FIELD_STATUS['TO_CHECK'],
          message: ''}})})});
      }
    }
  }

	handleChange(event) {
    if (event.target.id === IDS_INFORMATIVA.informativa) {
      this.setState({
        consensi: Object.assign(
          this.state.consensi, {
          informativa: event.target.checked})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].nome) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            nome: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].cognome) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            cognome: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].sesso) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            sesso: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].dataDiNascita.giorno) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            dataDiNascita: Object.assign(
              this.state.titolare.informazioniPersonali.dataDiNascita, {
              giorno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].dataDiNascita.mese) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            dataDiNascita: Object.assign(
              this.state.titolare.informazioniPersonali.dataDiNascita, {
              mese: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].dataDiNascita.anno) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            dataDiNascita: Object.assign(
              this.state.titolare.informazioniPersonali.dataDiNascita, {
              anno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].nazioneDiNascita) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            nazioneDiNascita: event.target.value,
            provinciaDiNascita: '',
            localitaDiNascita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].provinciaDiNascita) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            provinciaDiNascita: event.target.value,
            localitaDiNascita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].localitaDiNascita) {
      console.log(event.target.value);
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            localitaDiNascita: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].codiceFiscale) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            codiceFiscale: event.target.value.toUpperCase()})})});   
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['INFORMAZIONI_PERSONALI'].cittadinanza) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          informazioniPersonali: Object.assign(
            this.state.titolare.informazioniPersonali, {
            cittadinanza: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['CONTATTI'].email) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          contatti: Object.assign(
            this.state.titolare.contatti, {
            email: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['CONTATTI'].ripetiEmail) {
			this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          contatti: Object.assign(
            this.state.titolare.contatti, {
            ripetiEmail: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['CONTATTI'].telefonoCellulare) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          contatti: Object.assign(
            this.state.titolare.contatti, {
            telefonoCellulare: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['CONTATTI'].telefonoAbitazione) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          contatti: Object.assign(
            this.state.titolare.contatti, {
            telefonoAbitazione: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['CONTATTI'].telefonoUfficio) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          contatti: Object.assign(
            this.state.titolare.contatti, {
            telefonoUfficio: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].nazione) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            nazione: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].provincia) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            provincia: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].localita) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            localita: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].cap) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            cap: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].recapito.tipo) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            recapito: Object.assign(
              this.state.titolare.residenza.recapito, {
              tipo: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].recapito.indirizzo) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            recapito: Object.assign(
              this.state.titolare.residenza.recapito, {
              indirizzo: event.target.value.toUpperCase()})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].recapito.numero) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            recapito: Object.assign(
              this.state.titolare.residenza.recapito, {
              numero: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA'].presso) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenza: Object.assign(
            this.state.titolare.residenza, {
            presso: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA_FISCALE'].nazione1) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenzaFiscale: Object.assign(
            this.state.titolare.residenzaFiscale, {
            nazione1: event.target.value,
            codiceFiscaleEstero1: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA_FISCALE'].codiceFiscaleEstero1) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenzaFiscale: Object.assign(
            this.state.titolare.residenzaFiscale, {
            codiceFiscaleEstero1: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA_FISCALE'].nazione2) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenzaFiscale: Object.assign(
            this.state.titolare.residenzaFiscale, {
            nazione2: event.target.value,
            codiceFiscaleEstero2: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA_FISCALE'].codiceFiscaleEstero2) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenzaFiscale: Object.assign(
            this.state.titolare.residenzaFiscale, {
            codiceFiscaleEstero2: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA_FISCALE'].nazione3) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenzaFiscale: Object.assign(
            this.state.titolare.residenzaFiscale, {
            nazione3: event.target.value,
            codiceFiscaleEstero3: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['RESIDENZA_FISCALE'].codiceFiscaleEstero3) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          residenzaFiscale: Object.assign(
            this.state.titolare.residenzaFiscale, {
            codiceFiscaleEstero3: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].nazione) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            nazione: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].provincia) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            provincia: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].localita) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            localita: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].cap) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            cap: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].recapito.tipo) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            recapito: Object.assign(
              this.state.titolare.domicilio.recapito, {
              tipo: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].recapito.indirizzo) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            recapito: Object.assign(
              this.state.titolare.domicilio.recapito, {
              indirizzo: event.target.value.toUpperCase()})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].recapito.numero) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            recapito: Object.assign(
              this.state.titolare.domicilio.recapito, {
              numero: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOMICILIO'].presso) {
      console.log('presso');
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          domicilio: Object.assign(
            this.state.titolare.domicilio, {
            presso: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].tipo) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            tipo: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].numero) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            numero: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].autorita) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            autorita: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].nazione) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            nazione: event.target.value,
            provincia: '',
            localita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].provincia) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            provincia: event.target.value,
            localita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].localita) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            localita: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiEmissione.giorno) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            dataDiEmissione: Object.assign(
              this.state.titolare.documento.dataDiEmissione, {
              giorno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiEmissione.mese) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            dataDiEmissione: Object.assign(
              this.state.titolare.documento.dataDiEmissione, {
              mese: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiEmissione.anno) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            dataDiEmissione: Object.assign(
              this.state.titolare.documento.dataDiEmissione, {
              anno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiScadenza.giorno) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            dataDiScadenza: Object.assign(
              this.state.titolare.documento.dataDiScadenza, {
              giorno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiScadenza.mese) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            dataDiScadenza: Object.assign(
              this.state.titolare.documento.dataDiScadenza, {
              mese: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TITOLARE']['DOCUMENTO'].dataDiScadenza.anno) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          documento: Object.assign(
            this.state.titolare.documento, {
            dataDiScadenza: Object.assign(
              this.state.titolare.documento.dataDiScadenza, {
              anno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].nome) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            nome: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].cognome) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            cognome: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].sesso) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            sesso: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].dataDiNascita.giorno) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            dataDiNascita: Object.assign(
              this.state.tutore.informazioniPersonali.dataDiNascita, {
              giorno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].dataDiNascita.mese) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            dataDiNascita: Object.assign(
              this.state.tutore.informazioniPersonali.dataDiNascita, {
              mese: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].dataDiNascita.anno) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            dataDiNascita: Object.assign(
              this.state.tutore.informazioniPersonali.dataDiNascita, {
              anno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].nazioneDiNascita) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            nazioneDiNascita: event.target.value,
            provinciaDiNascita: '',
            localitaDiNascita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].provinciaDiNascita) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            provinciaDiNascita: event.target.value,
            localitaDiNascita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].localitaDiNascita) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            localitaDiNascita: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].codiceFiscale) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            codiceFiscale: event.target.value.toUpperCase()})})});   
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['INFORMAZIONI_PERSONALI'].cittadinanza) {
			this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          informazioniPersonali: Object.assign(
            this.state.tutore.informazioniPersonali, {
            cittadinanza: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].nazione) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            nazione: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].provincia) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            provincia: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].localita) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            localita: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].cap) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            cap: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].recapito.tipo) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            recapito: Object.assign(
              this.state.tutore.residenza.recapito, {
              tipo: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].recapito.indirizzo) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            recapito: Object.assign(
              this.state.tutore.residenza.recapito, {
              indirizzo: event.target.value.toUpperCase()})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].recapito.numero) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            recapito: Object.assign(
              this.state.tutore.residenza.recapito, {
              numero: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA'].presso) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenza: Object.assign(
            this.state.tutore.residenza, {
            presso: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA_FISCALE'].nazione1) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenzaFiscale: Object.assign(
            this.state.tutore.residenzaFiscale, {
            nazione1: event.target.value,
            codiceFiscaleEstero1: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA_FISCALE'].codiceFiscaleEstero1) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenzaFiscale: Object.assign(
            this.state.tutore.residenzaFiscale, {
            codiceFiscaleEstero1: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA_FISCALE'].nazione2) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenzaFiscale: Object.assign(
            this.state.tutore.residenzaFiscale, {
            nazione2: event.target.value,
            codiceFiscaleEstero2: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA_FISCALE'].codiceFiscaleEstero2) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenzaFiscale: Object.assign(
            this.state.tutore.residenzaFiscale, {
            codiceFiscaleEstero2: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA_FISCALE'].nazione3) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenzaFiscale: Object.assign(
            this.state.tutore.residenzaFiscale, {
            nazione3: event.target.value,
            codiceFiscaleEstero3: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['RESIDENZA_FISCALE'].codiceFiscaleEstero3) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          residenzaFiscale: Object.assign(
            this.state.tutore.residenzaFiscale, {
            codiceFiscaleEstero3: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].tipo) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            tipo: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].numero) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            numero: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].autorita) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            autorita: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].nazione) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            nazione: event.target.value,
            provincia: '',
            localita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].provincia) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            provincia: event.target.value,
            localita: ''})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].localita) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            localita: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiEmissione.giorno) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            dataDiEmissione: Object.assign(
              this.state.tutore.documento.dataDiEmissione, {
              giorno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiEmissione.mese) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            dataDiEmissione: Object.assign(
              this.state.tutore.documento.dataDiEmissione, {
              mese: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiEmissione.anno) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            dataDiEmissione: Object.assign(
              this.state.tutore.documento.dataDiEmissione, {
              anno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiScadenza.giorno) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            dataDiScadenza: Object.assign(
              this.state.tutore.documento.dataDiScadenza, {
              giorno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiScadenza.mese) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            dataDiScadenza: Object.assign(
              this.state.tutore.documento.dataDiScadenza, {
              mese: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PERSONALI['TUTORE']['DOCUMENTO'].dataDiScadenza.anno) {
      this.setState({
        tutore: Object.assign(
          this.state.tutore, {
          documento: Object.assign(
            this.state.tutore.documento, {
            dataDiScadenza: Object.assign(
              this.state.tutore.documento.dataDiScadenza, {
              anno: event.target.value})})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.abi) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            abi: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.cab) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            cab: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.agenzia) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            agenzia: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.cin) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            cin: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.professione) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            professione: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.fasciaReddito) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            fasciaReddito: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.tipoAttivitaEconomica) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            tipoAttivitaEconomica: event.target.value})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.datoreLavoro) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            datoreLavoro: event.target.value.toUpperCase()})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.provincia) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            provincia: event.target.value,
            nazione: ''})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.nazione) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            nazione: event.target.value,
            provincia: ''})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.reddito ||
               event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.pensione ||
               event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.eredita ||
               event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.rendita ||
               event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.vincita ||
               event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.donazione) {
      var provenienze = this.state.titolare.datiProfessionali.fondi.provenienze;
      if (event.target.checked) {
        provenienze.add(event.target.value);
      } else {
        provenienze.delete(event.target.value);
      }
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            fondi: Object.assign(
              this.state.titolare.datiProfessionali.fondi, {
                provenienze: provenienze})})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.altro) {
      var fondi = this.state.titolare.datiProfessionali.fondi;
      if (event.target.checked) {
        fondi.provenienze.add(event.target.value);
      } else {
        fondi.provenienze.delete(event.target.value);
        fondi.altraProvenienza = '';
      }
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            fondi: Object.assign(
              this.state.titolare.datiProfessionali.fondi, {
                provenienze: fondi.provenienze,
                altraProvenienza: fondi.altraProvenienza})})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.provenienzaFondi.altraProvenienza) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            fondi: Object.assign(
              this.state.titolare.datiProfessionali.fondi, {
                altraProvenienza: event.target.value.toUpperCase()})})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.finalitaRapporto.acquistiEsercizi ||
               event.target.id === IDS_DATI_PROFESSIONALI.finalitaRapporto.prelievi ||
               event.target.id === IDS_DATI_PROFESSIONALI.finalitaRapporto.acquistiInternet) {
      var finalitaRapporto = this.state.titolare.datiProfessionali.finalitaRapporto;
      if (event.target.checked) {
        finalitaRapporto.add(event.target.value);
      } else {
        finalitaRapporto.delete(event.target.value);
      }
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            finalitaRapporto: finalitaRapporto})})});
    } else if (event.target.id === IDS_DATI_PROFESSIONALI.esposizionePolitica) {
      this.setState({
        titolare: Object.assign(
          this.state.titolare, {
          datiProfessionali: Object.assign(
            this.state.titolare.datiProfessionali, {
            esposizionePolitica: event.target.value})})});
    } else if (event.target.id === IDS_RIEPILOGO.trattamentoDatiPersonali) {
      this.setState({
        consensi: Object.assign(
          this.state.consensi, {
          trattamentoDatiPersonali: event.target.checked})});
    } else if (event.target.id === IDS_RIEPILOGO.C3) {
      this.setState({
        consensi: Object.assign(
          this.state.consensi, {
          C3: (event.target.value === VALUES_IDS_RIEPILOGO['CONSENSO'].accettazione)})});
    } else if (event.target.id === IDS_RIEPILOGO.C4) {
      this.setState({
        consensi: Object.assign(
          this.state.consensi, {
          C4: (event.target.value === VALUES_IDS_RIEPILOGO['CONSENSO'].accettazione)})});
    } else if (event.target.id === IDS_RIEPILOGO.C5) {
      this.setState({
        consensi: Object.assign(
          this.state.consensi, {
          C5: (event.target.value === VALUES_IDS_RIEPILOGO['CONSENSO'].accettazione)})});
    }
	}

  handleDownloadContractClick(event) {
    event.preventDefault();
    fetch(getBaseUrl() + '/scaricaContratto.do', {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    .catch(error => {
      if (hasValue(error.titolo) && hasValue(error.messaggio)) {
        this.props.handleRedirect(PATHS['errore'], {titolo: error.titolo, messaggio: error.messaggio});
      } else {
        this.props.handleRedirect(PATHS['errore']);
      }
    });
  }

  handleProseguiClick(event) {
    event.preventDefault();

    fetch(getBaseUrl() + '/verificaStep.do', {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: serialize(this.state)
    })
    .then(response => {
      var json = response.json();
      if (!response.ok) {
        if (hasValue(MESSAGE[response.status]))
          throw {titolo: MESSAGE[response.status].titolo, messaggio: MESSAGE[response.status].messaggio}
      }
      return json;
    })
    .then(data => {
      if (data.esito === ESITO.OK) {
        console.log('ESITO OK');
        var currentStepNumber = this.state.currentStepNumber;
        var newNumber = currentStepNumber + 1;
        this.setState({
          currentStepNumber: newNumber,
          errori: []
        });
      } else if (data.esito === ESITO.KO) {
        console.log('ESITO KO');
        var errori = new Set();
        var message;
        for (let item of data.errori) {
          message = FIELD_LABELS[item.campo]['label'] + ' - ' + item.descrizione;
          errori.add(message);
        }
        this.setState({
          errori: Array.from(errori)
        });
      } else {
        console.log('ESITO UNKNOWN')
      }
    })
    .catch(error => {
      if (hasValue(error.titolo) && hasValue(error.messaggio)) {
        this.props.handleRedirect(PATHS['errore'], {titolo: error.titolo, messaggio: error.messaggio});
      } else {
        this.props.handleRedirect(PATHS['errore']);
      }
    });
  }

  handleIndietroClick(event) {
    event.preventDefault();
    var currentStepNumber = this.state.currentStepNumber;
    var newNumber = currentStepNumber - 1;
    this.setState({
      currentStepNumber: newNumber
    });
  }

  handleModificaClick(event) {
    event.preventDefault();
    var step = this.state.steps.indexOf('DATI_PERSONALI');
    this.setState({
      currentStepNumber: (step+1)
    });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    console.log(this.props.location);
    /*MAIN section*/
    var sezione;
    var currentStepLabel = this.state.steps[this.state.currentStepNumber-1];
    var currentStep = STEP_CONFIGURATION[currentStepLabel];
    var sezioneErrori = null;
    if (this.state.errori.length === 0) {
      sezioneErrori = null;
    } else {
      var lista = this.state.errori.map((text, index) => {
        return (
          <li key={index}>{text}</li>
        );
      });
      var sezioneErrori = <FormErrors errorList={lista} />
    }

    switch(currentStepLabel) {
      case 'INFORMATIVA':
        var buttons = [];
        if (STEP_CONFIGURATION['INFORMATIVA'].navigateBack) {
          buttons = buttons.concat(<IndietroButton key='backButton' onClick={this.handleIndietroClick} />);
        }
        if (STEP_CONFIGURATION['INFORMATIVA'].navigateNext) {
          buttons = buttons.concat(<ProseguiButton key='nextButton' onClick={this.handleProseguiClick} />);
        }
        sezione = <PaginaInformativa consensi={this.state.consensi} errori={sezioneErrori} navigationSection={buttons} handleChange={this.handleChange} />;
        break;
      case 'DATI_PERSONALI':
        var buttons = [];
        if (STEP_CONFIGURATION['DATI_PERSONALI'].navigateBack) {
          buttons = buttons.concat(<IndietroButton key='backButton' onClick={this.handleIndietroClick} />);
        }
        if (STEP_CONFIGURATION['DATI_PERSONALI'].navigateNext) {
          buttons = buttons.concat(<ProseguiButton key='nextButton' onClick={this.handleProseguiClick} />);
        }
        sezione = <FormDatiPersonali tipoCarta={this.props.tipoCarta} datiTitolare={this.state.titolare} datiTutore={this.state.tutore} errori={sezioneErrori} navigationSection={buttons} handleChange={this.handleChange} handleFocusOut={this.handleFocusOut} />;
        break;
      case 'DATI_PROFESSIONALI':
        var buttons = [];
        if (STEP_CONFIGURATION['DATI_PROFESSIONALI'].navigateBack) {
          buttons = buttons.concat(<IndietroButton key='backButton' onClick={this.handleIndietroClick} />);
        }
        if (STEP_CONFIGURATION['DATI_PROFESSIONALI'].navigateNext) {
          buttons = buttons.concat(<ProseguiButton key='nextButton' onClick={this.handleProseguiClick} />);
        }
        sezione = <FormDatiProfessionali tipoCarta={this.props.tipoCarta} datiProfessionali={this.state.titolare.datiProfessionali} errori={sezioneErrori} navigationSection={buttons} handleChange={this.handleChange} />;
        break;
      case 'RIEPILOGO':
        var buttons = [];
        if (STEP_CONFIGURATION['RIEPILOGO'].navigateBack) {
          buttons = buttons.concat(<IndietroButton key='backButton' onClick={this.handleIndietroClick} />);
        }
        if (STEP_CONFIGURATION['RIEPILOGO'].navigateNext) {
          buttons = buttons.concat(<ProseguiButton key='nextButton' onClick={this.handleProseguiClick} />);
        }
        sezione = <FormRiepilogo tipoCarta={this.props.tipoCarta} datiTitolare={this.state.titolare} datiTutore={this.state.tutore} consensi={this.state.consensi} errori={sezioneErrori} navigationSection={buttons} handleChange={this.handleChange} handleModifica={this.handleModificaClick} />;
        break;
      case 'FINALIZZAZIONE':
        var buttons = [];
        if (STEP_CONFIGURATION['FINALIZZAZIONE'].navigateBack) {
          buttons = buttons.concat(<IndietroButton key='backButton' onClick={this.handleIndietroClick} />);
        }
        if (STEP_CONFIGURATION['FINALIZZAZIONE'].navigateNext) {
          buttons = buttons.concat(<ProseguiButton key='nextButton' onClick={this.handleProseguiClick} />);
        }
        var button = <DownloadContractButton  onClick={this.handleDownloadContractClick} />
        sezione = <Finalizzazione tipoCarta={this.props.tipoCarta} errori={sezioneErrori} downloadButton={button} />;
        break;
      case 'FINE':
        var buttons = [];
        if (STEP_CONFIGURATION['FINE'].navigateBack) {
          buttons = buttons.concat(<IndietroButton key='backButton' onClick={this.handleIndietroClick} />);
        }
        if (STEP_CONFIGURATION['FINE'].navigateNext) {
          buttons = buttons.concat(<ProseguiButton key='nextButton' onClick={this.handleProseguiClick} />);
        }
        sezione = <Fine navigationSection={buttons} />;
        break;
      default:
    }

    return (
      <div className='column columnWide'>
        <div id='gadgetHeader'>
          Richiesta Carta Nexi Prepaid
        </div>
        <StepBar activeStep={STEP_CONFIGURATION[currentStepLabel]} />
        <div id='mainSection'>
          {sezione}
        </div>
        <div id='rightColumn' className='mtop60 atop'>
          <p className='boxTitle fnormal fsize18'>Carta Nexi Prepaid</p>
          <p className='boxBody' />
          <img src={imgNexiPrepaidFronte} className='width100' alt='img_nexi_prepaid_fronte' />
          <br /><br />
          <p className='boxTitle fnormal fsize18'>Trasparenza e Privacy</p>
          <p className='boxBody'>
          <ul className='whiteArrowList'>
            <li>
              <a title='Documento di sintesi Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Documento di sintesi Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Foglio informativo Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Foglio informativo Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Informativa in materia di trattamento dei dati personali Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Informativa in materia di trattamento dei dati personali Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Regolamento titolari Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Regolamento titolari Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Guida arbitro bancario e finanziario' href='#' target='_blank'>Guida arbitro bancario e finanziario</a>
            </li>
          </ul>
          </p>
        </div>
      </div>
    );
  }
}

export const Richiesta = withRouter(RichiestaWoRouter)