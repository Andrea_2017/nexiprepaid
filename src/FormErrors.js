import React from 'react';
import PropTypes from 'prop-types';
import Redirect from 'react-router';

export class FormErrors extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
			<ul className='errorList'>
				{this.props.errorList}
			</ul>
    );
  }
}