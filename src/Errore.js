import React from 'react';

export class Errore extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='gContentSection'>
        <p className='fbold'>{this.props.titolo}</p>
        <p>{this.props.messaggio}</p>
        <a href="<%=RETURN_URI%>">Continua</a>
      </div>
    );
  }
}