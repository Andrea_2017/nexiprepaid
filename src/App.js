import React from 'react';
import { Main } from './Main';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
  Link
} from 'react-router-dom';
import MetaTags from 'react-meta-tags';
import {Richiesta} from './Richiesta';

export class App extends React.Component {
  constructor(props) {
    super(props);
    console.log(Array(50).join("-") + '\nApp.render');
  }

  render() {
    console.log('App.render START ->')
    console.log(this.props.location);
    console.log('App.render END ->')

    return (
      <div>
        {/*
        <MetaTags>
          <script src="./iePolyfill.js" type="text/javascript"></script>
          <script src="./es5-shim.js" type="text/javascript"></script>
          <script src="./es6-shim.js" type="text/javascript"></script>
          <link rel="stylesheet" href="https://portal.cartasi.it/imageserver/plumtree/common/public/css/mainstyle-banche-it.css" type="text/css" />
        </MetaTags>
        */}
        <Main />
      </div>
    );
  }
}