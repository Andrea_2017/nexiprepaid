import React from 'react';
import PropTypes from 'prop-types';
import Redirect from 'react-router';
import {FormErrors} from './FormErrors';

export const FORM_IDS = {
  informativa: 'consensoInformativa'
};

{/*
const errori = ['Nome* - Campo obbligatorio', 'Cognome* - Campo obbligatorio'];
*/}

export class PaginaInformativa extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='sectionMargins'>
        <p className='sectionTitle'>Carta Nexi Prepaid</p>
        <form id='formInformativa'>
          <p>
            Stai avviando la richiesta di Carta Nexi Prepaid, la nuova prepagata Nexi. Prima di iniziare la richiesta online, 
            ti raccomandiamo di <strong>prendere visione dell'informativa pre-contrattuale</strong> che trovi in questa pagina.<br/>
            Una volta terminata con successo la procedura online, <strong>stampa il contratto e segui le istruzioni per ricevere Carta Nexi Prepaid
            all'indirizzo di residenza</strong> che hai inserito in fase di richiesta.<br/>
            <br/>
            La moneta elettronica rappresentata sulla carta è emessa dall'Istituto Centrale delle Banche Popolari Italiane S.p.A. Albo 
            dei Gruppi Bancari n.5000.5<br/>
            <br/>
            Circuito di accettazione: Visa.<br/>
            <br/><br/>
            Quota di emissione Nexi Prepaid: 7 euro.
          </p>
          {this.props.errori}
          <p className='subSectionBottom' />
          <p className='formInformativaSectionTitle'>Da tenere a portata di mano</p>
          <p>
            Per velocizzare il processo di richiesta online tieni vicino a te:
          </p>
          <ul className='squareList'>
            <li>Documento di identità (Carta di identità, Passaporto, Patente)
            </li>
            <li>Tesserino del codice fiscale rilasciato dall'Agenzia delle Entrate oppure tessera sanitaria.
            </li>
          </ul>
          <p>
            Ti ricordiamo che ricevuta la carta dovrai attivarla con una ricarica tramite bonifico, effettuato dal tuo conto corrente presso una Banca italiana, sull'IBAN riportato sul retro della carta stessa.<br />
          </p>
          <br/><br/>
          <p className='subSectionBottom' />
          <p className='formInformativaSectionTitle'>Documentazione pre-contrattuale</p>
          <p>
            Grazie a questi documenti puoi avere una visione completa dell'informativa legata al prodotto:
          </p>
          <ul className='whiteArrowList'>
            <li>
              <a title='Documento di sintesi Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Documento di sintesi Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Foglio informativo Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Foglio informativo Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Informativa in materia di trattamento dei dati personali Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Informativa in materia di trattamento dei dati personali Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Regolamento titolari Carta Nexi Prepaid Nominativa' href='#' target='_blank'>Regolamento titolari Carta Nexi Prepaid Nominativa</a>
            </li>
            <li>
              <a title='Guida arbitro bancario e finanziario' href='#' target='_blank'>Guida arbitro bancario e finanziario</a>
            </li>
          </ul>
          <br/>
          <p className='sectionInformativa'>
            <input id={FORM_IDS.informativa} type='checkbox' checked={this.props.consensi.informativa} onChange={this.props.handleChange} />
            <label>Ho preso visione dell'informativa pre-contrattuale</label>
            <span className='formMandatoryField'>*</span>
          </p>
          <br/><br/>
          <p className='sectionNavigate'>
              {this.props.navigationSection}
          </p>
          <br/><br/>
          <p>
            <span className='formMandatoryField'>*</span>
            <span> Informazione obbligatoria</span>
          </p>
        </form>
      </div>
    );
  }
}