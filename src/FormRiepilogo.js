import React from 'react';
import {formatItems, addBlockItem} from './Utils';
import {TIPOLOGIE_CARTA} from './Constants';
import {FORM_VALUES as VALUES_DATI_PROFESSIONALI} from './FormDatiProfessionali';
import {retrieveFondiValues} from './Richiesta';

import {MAP_LOCALITA, MAP_PROVINCE, MAP_ATTIVITA, MAP_REDDITO, MAP_PROFESSIONI, MAP_RESIDENZE_FISCALI, MAP_TIPI_DOCUMENTO} from './Constants';

export const FORM_IDS = {
  trattamentoDatiPersonali: 'consensoTrattamentoDatiPersonali',
  C3: 'consensoC3',
  C4: 'consensoC4',
  C5: 'consensoC5'
};

export const FORM_VALUES = {
  CONSENSO: {accettazione: 'ACCETTAZIONE',
             diniego: 'DINIEGO'}
};

function formatList(items) {
  var lista = items.map(function(fondo, index) {
    return (
      <span>*{fondo}</span>
    );
  });
  return addBlockItem(lista, <br/>);
}

class ModificaButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button className='modificaButton' onClick={this.props.onClick}>
        Modifica
      </button>
    );
  }
}

class RiepilogoInformazioniTitolare extends React.Component {
  constructor(props) {
		super(props);
  }
  render() {
    var riepilogoInformazioniProfessionali = null;
    if (this.props.tipoCarta === TIPOLOGIE_CARTA['NON_CONTRATTUALIZZATA']) {
      riepilogoInformazioniProfessionali = null;
    } else {
      riepilogoInformazioniProfessionali = <RiepilogoInformazioniProfessionali dati={this.props.datiTitolare.datiProfessionali} />;
    }

    var sectionResidenzaFiscale2 = null;
    if (this.props.datiTitolare.residenzaFiscale.nazione2 !== '') {
      sectionResidenzaFiscale2 = [
            <div className='dblock bheight10'/>,
            <p>Nazione:</p>,
            <p className='fieldRiepilogo'>
              {MAP_RESIDENZE_FISCALI[this.props.datiTitolare.residenzaFiscale.nazione2]}
            </p>,
            <div className='dblock bheight10'/>,
            <p>Codice Fiscale Estero:</p>,
            <p className='fieldRiepilogo'>
              {this.props.datiTitolare.residenzaFiscale.codiceFiscaleEstero2}
            </p>];
    }

    var sectionResidenzaFiscale3 = null;
    if (this.props.datiTitolare.residenzaFiscale.nazione3 !== '') {
      sectionResidenzaFiscale3 = [
            <div className='dblock bheight10'/>,
            <p>Nazione:</p>,
            <p className='fieldRiepilogo'>
              {MAP_RESIDENZE_FISCALI[this.props.datiTitolare.residenzaFiscale.nazione3]}
            </p>,
            <div className='dblock bheight10'/>,
            <p>Codice Fiscale Estero:</p>,
            <p className='fieldRiepilogo'>
              {this.props.datiTitolare.residenzaFiscale.codiceFiscaleEstero3}
            </p>];
    }

    var sectionResidenzaFiscale = [
      <div className='width98'>
        <p className='riepilogoSectionTitle'>Residenza Fiscale</p>
        <div className='riepilogoSectionBody'>
          <p>Nazione:</p>
          <p className='fieldRiepilogo'>
            {MAP_RESIDENZE_FISCALI[this.props.datiTitolare.residenzaFiscale.nazione1]}
          </p>
          <div className='dblock bheight10'/>
          <p>Codice Fiscale Estero:</p>
          <p className='fieldRiepilogo'>
            {this.props.datiTitolare.residenzaFiscale.codiceFiscaleEstero1}
          </p>
          {sectionResidenzaFiscale2}
          {sectionResidenzaFiscale3}
        </div>
      </div>];

    return (
      <div id='formRiepilogo'>
        <div className='formSectionColumn atop width50'>
          <div className='width98'>
            <p className='riepilogoSectionTitle'>Informazioni personali</p>
            <div className='riepilogoSectionBody'>
              <p>Nome:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.informazioniPersonali.nome}
              </p>
              <div className='dblock bheight10'/>
              <p>Cognome:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.informazioniPersonali.cognome}
              </p>
              <div className='dblock bheight10'/>
              <p>Sesso:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.informazioniPersonali.sesso}
              </p>
              <div className='dblock bheight10'/>
              <p>Data di nascita:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTitolare.informazioniPersonali.dataDiNascita.giorno,
                              this.props.datiTitolare.informazioniPersonali.dataDiNascita.mese,
                              this.props.datiTitolare.informazioniPersonali.dataDiNascita.anno], '  ')}
              </p>
              <div className='dblock bheight10'/>
              <p>Nazione di nascita:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.informazioniPersonali.nazioneDiNascita}
              </p>
              <div className='dblock bheight10'/>
              <p>Provincia di nascita:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.informazioniPersonali.provinciaDiNascita}
              </p>
              <div className='dblock bheight10'/>
              <p>Localit&agrave; di nascita:</p>
              <p className='fieldRiepilogo'>
                {(this.props.datiTitolare.informazioniPersonali.nazioneDiNascita === 'ITALIA') ?
                     MAP_LOCALITA[this.props.datiTitolare.informazioniPersonali.localitaDiNascita] : this.props.datiTitolare.informazioniPersonali.localitaDiNascita}
              </p>
              <div className='dblock bheight10'/>
              <p>Codice fiscale:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.informazioniPersonali.codiceFiscale}
              </p>
              <div className='dblock bheight10'/>
              <p>Cittadinanza:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.informazioniPersonali.cittadinanza}
              </p>
            </div>
          </div>
          <p className='formFieldVerticalSeparator' />
          <div className='width98'>
            <p className='riepilogoSectionTitle'>Residenza</p>
            <div className='riepilogoSectionBody'>
              <p>Presso (opzionale):</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.residenza.presso}
              </p>
              <div className='dblock bheight10'/>
              <p>Indirizzo:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTitolare.residenza.recapito.tipo,
                              this.props.datiTitolare.residenza.recapito.indirizzo,
                              this.props.datiTitolare.residenza.recapito.numero], ' ')}
              </p>
              <div className='dblock bheight10'/>
              <p>Nazione:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.residenza.nazione}
              </p>
              <div className='dblock bheight10'/>
              <p>Provincia:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.residenza.provincia}
              </p>
              <div className='dblock bheight10'/>
              <p>Localit&agrave;:</p>
              <p className='fieldRiepilogo'>
                {MAP_LOCALITA[this.props.datiTitolare.residenza.localita]}
              </p>
              <div className='dblock bheight10'/>
              <p>C.A.P.:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.residenza.cap}
              </p>
            </div>
          </div>
          <p className='formFieldVerticalSeparator' />
          {sectionResidenzaFiscale}
          <p className='formFieldVerticalSeparator' />
          <div className='width98'>
            <p className='riepilogoSectionTitle'>Domicilio</p>
            <div className='riepilogoSectionBody'>
              <p>Presso (opzionale):</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.domicilio.presso}
              </p>
              <div className='dblock bheight10'/>
              <p>Indirizzo:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTitolare.domicilio.recapito.tipo,
                              this.props.datiTitolare.domicilio.recapito.indirizzo,
                              this.props.datiTitolare.domicilio.recapito.numero], ' ')}
              </p>
              <div className='dblock bheight10'/>
              <p>Nazione:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.domicilio.nazione}
              </p>
              <div className='dblock bheight10'/>
              <p>Provincia:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.domicilio.provincia}
              </p>
              <div className='dblock bheight10'/>
              <p>Localit&agrave;:</p>
              <p className='fieldRiepilogo'>
                {MAP_LOCALITA[this.props.datiTitolare.domicilio.localita]}
              </p>
              <div className='dblock bheight10'/>
              <p>C.A.P.:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.domicilio.cap}
              </p>
            </div>
          </div>
          <p className='formFieldVerticalSeparator' />
          <div className='width98'>
            <p className='riepilogoSectionTitle'>Contatti</p>
            <div className='riepilogoSectionBody'>
              <p>E-mail:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.contatti.email}
              </p>
              <div className='dblock bheight10'/>
              <p>Numero di cellulare:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.contatti.telefonoCellulare}
              </p>
              <div className='dblock bheight10'/>
              <p>Telefono abitazione:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.contatti.telefonoAbitazione}
              </p>
              <div className='dblock bheight10'/>
              <p>Telefono ufficio:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.contatti.telefonoUfficio}
              </p>
            </div>
          </div>
        </div>
        <div className='formSectionColumn atop width50'>
          <div className='fright width98'>
            <p className='riepilogoSectionTitle'>Documento</p>
            <div className='riepilogoSectionBody'>
              <p>Tipo di documento:</p>
              <p className='fieldRiepilogo'>
                {MAP_TIPI_DOCUMENTO[this.props.datiTitolare.documento.tipo]}
              </p>
              <div className='dblock bheight10'/>
              <p>Numero documento:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.documento.numero}
              </p>
              <div className='dblock bheight10'/>
              <p>Ente di rilascio:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.documento.autorita}
              </p>
              <div className='dblock bheight10'/>
              <p>Nazione di rilascio:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.documento.nazione}
              </p>
              <div className='dblock bheight10'/>
              <p>Provincia di rilascio:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTitolare.documento.provincia}
              </p>
              <div className='dblock bheight10'/>
              <p>Localit&agrave; di rilascio:</p>
              <p className='fieldRiepilogo'>
                {(this.props.datiTitolare.documento.nazione === 'ITALIA') ? 
                      MAP_LOCALITA[this.props.datiTitolare.documento.localita] : this.props.datiTitolare.documento.localita}
              </p>
              <div className='dblock bheight10'/>
              <p>Data ultimo rinnovo:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTitolare.documento.dataDiEmissione.giorno,
                              this.props.datiTitolare.documento.dataDiEmissione.mese,
                              this.props.datiTitolare.documento.dataDiEmissione.anno], '  ')}
              </p>
              <div className='dblock bheight10'/>
              <p>Data scadenza:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTitolare.documento.dataDiScadenza.giorno,
                              this.props.datiTitolare.documento.dataDiScadenza.mese,
                              this.props.datiTitolare.documento.dataDiScadenza.anno], '  ')}
              </p>
            </div>
          </div>
          <div className='clear' />
          <div className='formFieldVerticalSeparator' />
          {riepilogoInformazioniProfessionali}
        </div>
      </div>
    );
  }
}

class RiepilogoInformazioniProfessionali extends React.Component {
  constructor(props) {
		super(props);
  }
  
  render() {
    return (
      <div className='fright width98'>
        <p className='riepilogoSectionTitle'>Informazioni professionali</p>
        <div className='riepilogoSectionBody'>
          <p>Professione:</p>
          <p className='fieldRiepilogo'>
            {MAP_PROFESSIONI[this.props.dati.professione]}
          </p>
          <div className='dblock bheight10'/>
          <p>Fascia di reddito:</p>
          <p className='fieldRiepilogo'>
            {MAP_REDDITO[this.props.dati.fasciaReddito]}
          </p>
          <div className='dblock bheight10'/>
          <p>Tipo di attivit&agrave;<br />economica:</p>
          <p className='fieldRiepilogo'>
            {MAP_ATTIVITA[this.props.dati.tipoAttivitaEconomica]}
          </p>
          <div className='dblock bheight10'/>
          <p>Datore di lavoro:</p>
          <p className='fieldRiepilogo'>
            {this.props.dati.datoreLavoro}
          </p>
          <div className='dblock bheight10'/>
          <p>Luogo prevalente di<br />svolgimento<br />dell'attivit&agrave;<br />economica:</p>
          <p className='fieldRiepilogo'>
            {(this.props.dati.provincia !== '') ? MAP_PROVINCE[this.props.dati.provincia]
                : this.props.dati.nazione}
          </p>
          <div className='dblock bheight10'/>
          <p>Provenienza fondi:</p>
          <p className='fieldRiepilogo'>
            {formatList(retrieveFondiValues(this.props.dati.fondi))}
          </p>
          <div className='dblock bheight10'/>
          <p>Finalit&agrave; del rapporto:</p>
          <p className='fieldRiepilogo'>
            {formatList(Array.from(this.props.dati.finalitaRapporto))}
          </p>
        </div>
      </div>
    );
  }
}

class RiepilogoInformazioniTutore extends React.Component {
  constructor(props) {
		super(props);
  }
  
  render() {
    var sectionResidenzaFiscale2 = null;
    if (this.props.datiTutore.residenzaFiscale.nazione2 !== '') {
      sectionResidenzaFiscale2 = [
            <div className='dblock bheight10'/>,
            <p>Nazione:</p>,
            <p className='fieldRiepilogo'>
              {MAP_RESIDENZE_FISCALI[this.props.datiTutore.residenzaFiscale.nazione2]}
            </p>,
            <div className='dblock bheight10'/>,
            <p>Codice Fiscale Estero:</p>,
            <p className='fieldRiepilogo'>
              {this.props.datiTutore.residenzaFiscale.codiceFiscaleEstero2}
            </p>];
    }

    var sectionResidenzaFiscale3 = null;
    if (this.props.datiTutore.residenzaFiscale.nazione3 !== '') {
      sectionResidenzaFiscale3 = [
            <div className='dblock bheight10'/>,
            <p>Nazione:</p>,
            <p className='fieldRiepilogo'>
              {MAP_RESIDENZE_FISCALI[this.props.datiTutore.residenzaFiscale.nazione3]}
            </p>,
            <div className='dblock bheight10'/>,
            <p>Codice Fiscale Estero:</p>,
            <p className='fieldRiepilogo'>
              {this.props.datiTutore.residenzaFiscale.codiceFiscaleEstero3}
            </p>];
    }

    var sectionResidenzaFiscale = [
      <div className='width98'>
        <p className='riepilogoSectionTitle'>Residenza Fiscale</p>
        <div className='riepilogoSectionBody'>
          <p>Nazione:</p>
          <p className='fieldRiepilogo'>
            {MAP_RESIDENZE_FISCALI[this.props.datiTutore.residenzaFiscale.nazione1]}
          </p>
          <div className='dblock bheight10'/>
          <p>Codice Fiscale Estero:</p>
          <p className='fieldRiepilogo'>
            {this.props.datiTutore.residenzaFiscale.codiceFiscaleEstero1}
          </p>
          {sectionResidenzaFiscale2}
          {sectionResidenzaFiscale3}
        </div>
      </div>];

    return (
      <div id='formRiepilogo'>
        <div className='subSectionDivider' />
        <div className='formSectionVerticalSeparator' />
        <div className='mleft100'>
          <div>
            <p className='sectionTitle dinlineblock'>Riepilogo dati tutore richiedente</p>
            <ModificaButton onClick={this.props.handleModifica} />
          </div>
          <p className='subSection'>
            Riepilogo delle informazioni riguardanti il tutore richiedente.
          </p>
        </div>
				<div className='formSectionVerticalSeparator' />
        <div className='formSectionColumn atop width50'>
          <div className='width98'>
            <p className='riepilogoSectionTitle'>Informazioni personali</p>
            <div className='riepilogoSectionBody'>
              <p>Nome:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.informazioniPersonali.nome}
              </p>
              <div className='dblock bheight10'/>
              <p>Cognome:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.informazioniPersonali.cognome}
              </p>
              <div className='dblock bheight10'/>
              <p>Sesso:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.informazioniPersonali.sesso}
              </p>
              <div className='dblock bheight10'/>
              <p>Data di nascita:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTutore.informazioniPersonali.dataDiNascita.giorno,
                              this.props.datiTutore.informazioniPersonali.dataDiNascita.mese,
                              this.props.datiTutore.informazioniPersonali.dataDiNascita.anno], '  ')}
              </p>
              <div className='dblock bheight10'/>
              <p>Nazione di nascita:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.informazioniPersonali.nazioneDiNascita}
              </p>
              <div className='dblock bheight10'/>
              <p>Provincia di nascita:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.informazioniPersonali.provinciaDiNascita}
              </p>
              <div className='dblock bheight10'/>
              <p>Localit&agrave; di nascita:</p>
              <p className='fieldRiepilogo'>
                {(this.props.datiTutore.informazioniPersonali.nazioneDiNascita === 'ITALIA') ?
                    MAP_LOCALITA[this.props.datiTutore.informazioniPersonali.localitaDiNascita] : this.props.datiTutore.informazioniPersonali.localitaDiNascita}
              </p>
              <div className='dblock bheight10'/>
              <p>Codice fiscale:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.informazioniPersonali.codiceFiscale}
              </p>
              <div className='dblock bheight10'/>
              <p>Cittadinanza:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.informazioniPersonali.cittadinanza}
              </p>
            </div>
          </div>
          <p className='formFieldVerticalSeparator' />
          <div className='width98'>
            <p className='riepilogoSectionTitle'>Residenza</p>
            <div className='riepilogoSectionBody'>
              <p>Presso (opzionale):</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.residenza.presso}
              </p>
              <div className='dblock bheight10'/>
              <p>Indirizzo:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTutore.residenza.recapito.tipo,
                              this.props.datiTutore.residenza.recapito.indirizzo,
                              this.props.datiTutore.residenza.recapito.numero], ' ')}
              </p>
              <div className='dblock bheight10'/>
              <p>Nazione:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.residenza.nazione}
              </p>
              <div className='dblock bheight10'/>
              <p>Provincia:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.residenza.provincia}
              </p>
              <div className='dblock bheight10'/>
              <p>Localit&agrave;:</p>
              <p className='fieldRiepilogo'>
                {MAP_LOCALITA[this.props.datiTutore.residenza.localita]}
              </p>
              <div className='dblock bheight10'/>
              <p>C.A.P.:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.residenza.cap}
              </p>
            </div>
          </div>
          <p className='formFieldVerticalSeparator' />
          {sectionResidenzaFiscale}
        </div>
        <div className='formSectionColumn atop width50'>
          <div className='fright width98'>
            <p className='riepilogoSectionTitle'>Documento</p>
            <div className='riepilogoSectionBody'>
              <p>Tipo di documento:</p>
              <p className='fieldRiepilogo'>
                {MAP_TIPI_DOCUMENTO[this.props.datiTutore.documento.tipo]}
              </p>
              <div className='dblock bheight10'/>
              <p>Numero documento:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.documento.numero}
              </p>
              <div className='dblock bheight10'/>
              <p>Ente di rilascio:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.documento.autorita}
              </p>
              <div className='dblock bheight10'/>
              <p>Nazione di rilascio:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.documento.nazione}
              </p>
              <div className='dblock bheight10'/>
              <p>Provincia di rilascio:</p>
              <p className='fieldRiepilogo'>
                {this.props.datiTutore.documento.provincia}
              </p>
              <div className='dblock bheight10'/>
              <p>Localit&agrave; di rilascio:</p>
              <p className='fieldRiepilogo'>
                {(this.props.datiTutore.documento.nazione === 'ITALIA') ? 
                    MAP_LOCALITA[this.props.datiTutore.documento.localita] : this.props.datiTutore.documento.localita}
              </p>
              <div className='dblock bheight10'/>
              <p>Data ultimo rinnovo:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTutore.documento.dataDiEmissione.giorno,
                              this.props.datiTutore.documento.dataDiEmissione.mese,
                              this.props.datiTutore.documento.dataDiEmissione.anno], '  ')}
              </p>
              <div className='dblock bheight10'/>
              <p>Data scadenza:</p>
              <p className='fieldRiepilogo'>
                {formatItems([this.props.datiTutore.documento.dataDiScadenza.giorno,
                              this.props.datiTutore.documento.dataDiScadenza.mese,
                              this.props.datiTutore.documento.dataDiScadenza.anno], '  ')}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export class FormRiepilogo extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
    var sectionRiepilogoTutore = null;
    if (this.props.tipoCarta === TIPOLOGIE_CARTA['CONTRATTUALIZZATA_MINORE']) {
      sectionRiepilogoTutore = <RiepilogoInformazioniTutore datiTutore={this.props.datiTutore} handleModifica={this.props.handleModifica} />;
    } else {
      sectionRiepilogoTutore = null;
    }

		return (
			<div>
        <div className='sectionMargins'>
          <div>
            <p className='sectionTitle dinlineblock'>Riepilogo dati titolare</p>
            <ModificaButton onClick={this.props.handleModifica} />
          </div>
          <p className='subSection'>
            Riepilogo delle informazioni riguardanti il titolare della carta.
          </p>
          {this.props.errori}
        </div>
				<div className='formSectionVerticalSeparator' />
        <RiepilogoInformazioniTitolare datiTitolare={this.props.datiTitolare} tipoCarta={this.props.tipoCarta} />
        <div className='formSectionVerticalSeparator' />
        {sectionRiepilogoTutore}
				<p className='formSectionVerticalSeparator' />
        <div className='sectionMargins'>
          <form id='formConsensoDatiPersonali'>
            <p className='fsize13 fbold'>Richiesta Consensi</p>
            <br />
            <p className='fsize12 fbolder'>CONSENSO PER IL TRATTAMENTO DEI DATI PERSONALI</p>
            <p>(Ai sensi della normativa vigente in materia di protezione dei dati personali)</p>
            <br />
            <p>
              In relazione all'Informativa che mi è stata fornita ai sensi della vigente normativa in materia di 
              protezione dei dati personali, prendo atto che l'esecuzione del contratto di emissione e gestione 
              della Carta da parte della Banca e di Nexi richiede il trattamento dei miei dati personali 
              nonch&eacute; la loro comunicazione alle categorie di soggetti indicati nell'Informativa stessa.
            </p>
            <p>
              Per quanto riguarda il trattamento dei miei dati sensibili derivante da specifiche operazioni o 
              servizi richiesti alla Banca e a Nexi, consapevole che in mancanza del mio consenso Nexi non potr&agrave; 
              dar corso a tale contratto ed emettere la Carta da me richiesta, acconsento al loro trattamento. 
            </p>
            <br />
            <p>
              <input id={FORM_IDS.trattamentoDatiPersonali} type='checkbox' checked={this.props.consensi.trattamentoDatiPersonali} onChange={this.props.handleChange} />
              <label className='fsize12 fbold'>Presto il consenso</label>
              <span className='formMandatoryField'>*</span>
            </p>
            <p className='formSectionVerticalSeparator' />
            <p className='fsize12 fbold'>Consenso C3</p>
            <p>
              Per quanto riguarda il trattamento dei miei dati a fini di informazione e promozione di prodotti e servizi 
              della Banca e di Nexi ovvero di prodotti o servizi di terzi mediante annunci commerciali veicolati attraverso 
              contatto telefonico, messaggi SMS, fax e posta elettronica e cartacea:
            </p>
            <br />
            <p className='formSectionColumn width30'>
              <input id={FORM_IDS.C3} type='radio' value={FORM_VALUES['CONSENSO'].accettazione} checked={(this.props.consensi.C3) ? true : false} onChange={this.props.handleChange} />
              <span className='fsize12 fbold'>Presto il consenso</span>
            </p>
            <p className='formSectionColumn width30'>
              <input id={FORM_IDS.C3} type='radio' value={FORM_VALUES['CONSENSO'].diniego} checked={(this.props.consensi.C3) ? false : true} onChange={this.props.handleChange} />
              <span className='fsize12 fbold'>Nego il consenso</span>
            </p>
            <p className='formSectionVerticalSeparator' />
            <p className='fsize12 fbold'>Consenso C4</p>
            <p>
              Per quanto riguarda il trattamento dei miei dati a fini di ricerche di mercato o di rilevazione del grado di 
              soddisfazione della clientela sulla qualit&agrave; dei servizi resi e sull'attivit&agrave; svolta dalla Banca 
              e da Nexi: tali indagini potranno essere svolte, direttamente dalle parti oppure da altri soggetti incaricati 
              dalle parti stesse attraverso contatto telefonico, messaggi SMS, fax e posta elettronica e cartacea:
            </p>
            <br />
            <p className='formSectionColumn width30'>
              <input id={FORM_IDS.C4} type='radio' value={FORM_VALUES['CONSENSO'].accettazione} checked={(this.props.consensi.C4) ? true : false} onChange={this.props.handleChange} />
              <span className='fsize12 fbold'>Presto il consenso</span>
            </p>
            <p className='formSectionColumn width30'>
              <input id={FORM_IDS.C4} type='radio' value={FORM_VALUES['CONSENSO'].diniego} checked={(this.props.consensi.C4) ? false : true} onChange={this.props.handleChange} />
              <span className='fsize12 fbold'>Nego il consenso</span>
            </p>
            <p className='formSectionVerticalSeparator' />
            <p className='fsize12 fbold'>Consenso C5</p>
            <p>
              Per quanto riguarda la comunicazione, da parte della Banca e di Nexi dei miei dati a societ&agrave; terze a fini 
              di offerte dirette di loro prodotti o servizi:
            </p>
            <br />
            <p className='formSectionColumn width30'>
              <input id={FORM_IDS.C5} type='radio' value={FORM_VALUES['CONSENSO'].accettazione} checked={(this.props.consensi.C5) ? true : false} onChange={this.props.handleChange} />
              <span className='fsize12 fbold'>Presto il consenso</span>
            </p>
            <p className='formSectionColumn width30'>
              <input id={FORM_IDS.C5} type='radio' value={FORM_VALUES['CONSENSO'].diniego} checked={(this.props.consensi.C5) ? false : true} onChange={this.props.handleChange} />
              <span className='fsize12 fbold'>Nego il consenso</span>
            </p>
          </form>
          <p className='formSectionVerticalSeparator' />
          <p className='sectionNavigate'>
              {this.props.navigationSection}
          </p>
        </div>
			</div>
		);
	}
}