import React from 'react';
import icoContratto from './images/end_download.png';
import icoDocumento from './images/end_fotocopia.png';
import icoFotocopia from './images/end_inviacontratto.png';
import icoCarta from './images/end_carta.png';
import icoBonifico from './images/end_bonifico.png';
import imgNexiPrepaidRetro from './images/nexi_prepaid_retro.png';

export class Finalizzazione extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			datiPersonali: {
				nome: '',
				cognome: '',
				sesso: 'Maschio'
			},
			tipo: 'form_richiesta_carta'
		};
	}

	render() {
		const datiPersonali = this.state.datiPersonali;
		return (
			<div className='sectionMargins'>
				<p className='sectionTitle'>Il processo di richiesta della Carta Nexi Prepaid &egrave; quasi concluso</p>
        <br />
        <div id='formFinalizzazione'>
          <p>
            Segui questi passi per ottenere la tua nuova carta.<br />
            <span className='fbold'>Importante:</span> questo &egrave; l'unico punto in cui 
            puoi visionare il tuo contratto, <span className='fbold'>scaricalo e salvalo subito sul tuo computer.</span>
          </p>
          <br />
          <p className='sectionNavigate'>
            {this.props.downloadButton}
          </p>
          <br />




          <p className='formSectionTitle'>
						1. Stampa e firma il contratto
					</p>
          <p className='subSectionBottom' />
          <br className='lheight5' />
          <p className='formSectionColumn atop width18'>
            <a href='#' target='_blank'>
              <img src={icoContratto} className='width38' alt='ico_contratto' />
            </a>
          </p>
          <ul className='formSectionColumn width70 squareList'>
            <li><a href='#'>Scarica il contratto</a></li>
            <li>Stampa il contratto</li>
            <li>Firma il contratto in tutte le sue parti</li>
          </ul>
          <p className='formSectionVerticalSeparator' />





          <p className='formSectionTitle'>
						2. Fotocopia (fronte e retro) i documenti seguenti:
					</p>
          <p className='subSectionBottom' />
          <br className='lheight5' />
          <p className='formSectionColumn atop width18'>
            <a href='#' target='_blank'>
              <img src={icoDocumento} className='width38' alt='ico_documento' />
            </a>
          </p>
          <ul className='formSectionColumn width70 squareList'>
            <li>Copia leggibile fronte/retro del documento di riconoscimento valido 
              utilizzato durante la compilazione per la richiesta della Carta Nexi Prepaid 
              (carta d'identit&agrave;, passaporto o patente).</li>
            <li>Copia leggibile del tesserino del Codice Fiscale rilasciato dell'Agenzia 
              delle Entrate oppure della Tessera Sanitaria</li>
          </ul>
          <p className='formSectionVerticalSeparator' />


          <p className='formSectionTitle'>
            3. Invia contratto e fotocopie al seguente indirizzo:
					</p>
          <p className='subSectionBottom' />
          <br className='lheight5' />
          <p className='formSectionColumn atop width18'>
            <a href='#' target='_blank'>
              <img src={icoFotocopia} className='width38' alt='ico_fotocopia' />
            </a>
          </p>
          <p className='formSectionColumn atop width70'>
            Nexi Payments SpA<br />
            Archivi Issuing<br />
            Corso Sempione, 55<br />
            20145 - Milano<br />
            Italia
          </p>
          <p className='formSectionVerticalSeparator' />


          <p className='formSectionTitle'>
            4. Aspetta di aver ricevuto la tua Carta Nexi Prepaid
					</p>
          <p className='subSectionBottom' />
          <br className='lheight5' />
          <p className='formSectionColumn atop width18'>
            <a href='#' target='_blank'>
              <img src={icoCarta} className='width38' alt='ico_carta' />
            </a>
          </p>
          <p className='formSectionColumn width70 squareList'>
            Quando Nexi Payments ricever&agrave; i documenti, ti invier&agrave; per posta la tua Carta Nexi Prepaid. 
          </p>
          <p className='formSectionVerticalSeparator' />


          <p className='formSectionTitle'>
						5. Esegui il bonifico di riconoscimento per attivare la carta
					</p>
          <p className='subSectionBottom' />
          <br className='lheight5' />
          <p className='formSectionColumn atop width18'>
            <a href='#' target='_blank'>
              <img src={icoBonifico} className='width38' alt='ico_bonifico' />
            </a>
          </p>
          <div className='formSectionColumn width70'>
            <p>Quando ti sar&agrave; arrivata la carta, effettua un bonifico (da un conto presso una Banca Italiana 
              che abbia come intestatario il titolare della carta) con i seguenti dati: 
            </p>
            <ul className='squareList'>
              <li>Beneficiario: Nome e Cognome del titolare della carta</li>
              <li>Causale di versamento:</li>
              <li>IBAN: Indicato sul retro della tua Carta Nexi Prepaid</li>
              <li>Importo minimo del bonifico: 25,00€</li>
            </ul>
            <img src={imgNexiPrepaidRetro} className='fright width50' alt='img_nexi_prepaid_retro' />
          </div>
          <p className='formSectionVerticalSeparator' />




          <p className='sectionNavigate'>
					  {this.props.downloadButton}
				  </p>
        </div>
      </div>
		);
	}
}