import React from 'react';
import PropTypes from 'prop-types';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  withRouter
} from 'react-router-dom';
import blocco from './images/bloccof.gif';
import {Vendita} from './Vendita';
import {Gestione} from './Gestione';
import {Informazioni} from './Informazioni';
import {Richiesta} from './Richiesta';
import {TestCss} from './TestCss';
import {PATHS, TIPOLOGIE_CARTA, MESSAGE} from './Constants';
import {Errore} from './Errore';
import {isUndefined, isNull, hasValue, getUrlVars} from './Utils';
import Popup from 'react-popup';

class MainNavigation extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props);
    this.handleRedirect = this.handleRedirect.bind(this);
  }

  handleRedirect(pathname, data) {
    console.log('handling redirect');
    var redirectState = null;
    if (pathname === PATHS['vendita']) {

    } else if (pathname === PATHS['gestione']) {
    } else if (pathname === PATHS['informazioni']) {
    } else if (pathname === PATHS['errore']) {
      if (hasValue(data) && hasValue(data.titolo) && hasValue(data.messaggio)) {
        console.log('titolo errore ' + data.titolo);
        console.log('titolo messaggio ' + data.messaggio);
        redirectState = {errore: {titolo: data.titolo, messaggio: data.messaggio}}
      } else {
        console.log('non ha valore!!!');
        redirectState = {errore: {titolo: MESSAGE['ERRORE_GENERICO']['titolo'], messaggio: MESSAGE['ERRORE_GENERICO']['messaggio']}}
      }
    } else if (pathname === PATHS['richiesta/contrattualizzata']) {    
    } else {
      console.log('pathname unknown');
    }
    this.props.history.push(
      {
        pathname: pathname,
        state: redirectState
      }
    );
  }

  render() {
    {/*
    var urlParams = getUrlVars();
    var sezione = null;
    var sidebarLink = null;
    switch (urlParams["sezione"]) {
      case "vendita":
        sezione = <Vendita />;
        sidebarLink = [
          <p><Link to='?sezione=gestione'>Gestione carta</Link></p>,
          <p><Link to='?sezione=informazioni'>Informazioni</Link></p>];
        break;
    
      case "gestione":
        sezione = <Gestione />;
        sidebarLink = [
          <p><Link to='?sezione=vendita'>Vendita</Link></p>,
          <p><Link to='?sezione=informazioni'>Informazioni</Link></p>];
        break;

      case "richiesta_contrattualizzata":
        sezione = <Richiesta tipoCarta={TIPOLOGIE_CARTA['CONTRATTUALIZZATA']} />;
        sidebarLink = [
          <p><Link to='?sezione=gestione'>Gestione carta</Link></p>,
          <p><Link to='?sezione=informazioni'>Informazioni</Link></p>];
        break;

      case "richiesta_contrattualizzata_minore":
        sezione = <Richiesta tipoCarta={TIPOLOGIE_CARTA['CONTRATTUALIZZATA_MINORE']} />;
        sidebarLink = [
          <p><Link to='?sezione=gestione'>Gestione carta</Link></p>,
          <p><Link to='?sezione=informazioni'>Informazioni</Link></p>];
        break;

      case "richiesta_non_contrattualizzata":
        sezione = <Richiesta tipoCarta={TIPOLOGIE_CARTA['NON_CONTRATTUALIZZATA']} />;
        sidebarLink = [
          <p><Link to='?sezione=gestione'>Gestione carta</Link></p>,
          <p><Link to='?sezione=informazioni'>Informazioni</Link></p>];
        break;

      case "errore":
        
        break;

      case "informazioni":
      case undefined:
      default:
        sezione = <Informazioni />;
        sidebarLink = [
          <p><Link to='?sezione=vendita'>Vendita</Link></p>,
          <p><Link to='?sezione=gestione'>Gestione carta</Link></p>];
    }
    */}

    return (
      <Switch>
        <Route path='/vendita' render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/gestione'>Gestione carta</Link></p>
              <p><Link to='/informazioni'>Informazioni</Link></p>
            </div>
            <Vendita handleRedirect={this.handleRedirect} />
          </div>
          )
        }/>
        <Route path='/gestione' render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/vendita'>Vendita</Link></p>
              <p><Link to='/informazioni'>Informazioni</Link></p>
            </div>
            <Gestione />
          </div>
          )
        }/>
        <Route path='/informazioni' render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/vendita'>Vendita</Link></p>
              <p><Link to='/gestione'>Gestione</Link></p>
            </div>
            <Informazioni />
          </div>
          )
        }/>
        <Route path='/richiesta/contrattualizzata' render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/gestione'>Gestione carta</Link></p>
              <p><Link to='/informazioni'>Informazioni</Link></p>
            </div>
            <Richiesta tipoCarta={TIPOLOGIE_CARTA['CONTRATTUALIZZATA']} handleRedirect={this.handleRedirect} />
          </div>
          )
        }/>
        <Route path='/richiesta/contrattualizzata/minore' render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/gestione'>Gestione carta</Link></p>
              <p><Link to='/informazioni'>Informazioni</Link></p>
            </div>
            <Richiesta tipoCarta={TIPOLOGIE_CARTA['CONTRATTUALIZZATA_MINORE']} handleRedirect={this.handleRedirect} />;
          </div>
          )
        }/>
        <Route path='/richiesta/noncontrattualizzata' render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/gestione'>Gestione carta</Link></p>
              <p><Link to='/informazioni'>Informazioni</Link></p>
            </div>
            <Richiesta tipoCarta={TIPOLOGIE_CARTA['NON_CONTRATTUALIZZATA']} handleRedirect={this.handleRedirect} />;
          </div>
          )
        }/>
        <Route path='/errore' render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/vendita'>Vendita</Link></p>
              <p><Link to='/gestione'>Gestione</Link></p>
            </div>
            <Errore titolo={(hasValue(this.props.location.state) && hasValue(this.props.location.state.errore))
                                    ? this.props.location.state.errore.titolo
                                    : MESSAGE['ERRORE_GENERICO']['titolo']}
                    messaggio={(hasValue(this.props.location.state) && hasValue(this.props.location.state.errore))
                                    ? this.props.location.state.errore.messaggio
                                    : MESSAGE['ERRORE_GENERICO']['messaggio']} />
          </div>
          )
        }/>
        <Route render={props => (
          <div className='gContentSection'>
            <div id='gadgetSidebar' className='column'>
              <img src={blocco} alt='immagine blocco f' />
              <p><Link to='/vendita'>Vendita</Link></p>
              <p><Link to='/gestione'>Gestione</Link></p>
            </div>
            <Informazioni />
          </div>
          )
        }/>
      </Switch>
    );
  }
}

export const Main = withRouter(MainNavigation);