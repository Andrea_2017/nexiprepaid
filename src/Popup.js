import React, {Component} from 'react'
import {Modal, Button, AlertContainer} from 'react-bootstrap';
 
export class Popup extends React.Component {
  render () {
    return (
      <div className="static-modal">
        <Modal.Dialog>
          <Modal.Body>
            <p>
              Prima di proseguire con la richiesta carta, inserisci nel box sotto
              il n. posizione che trovi all'interno della fustella e clicca Conferma
            </p>
            <input type='text' />
          </Modal.Body>
          <Modal.Footer>
          <button className='navigateIndietroButton' onClick={this.props.onClick}>
            Indietro
          </button>
          </Modal.Footer>
        </Modal.Dialog>
      </div>
    )
  }
}