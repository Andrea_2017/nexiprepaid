export function getUrlVars() {
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}

export function getBaseUrl() {
  var url = window.location.pathname.substring(0,window.location.pathname.lastIndexOf('prepaid'));
  url = url + 'prepaid';
  console.log('BASENAME: ' + url);
  return url;
}

export function setToString(set) {
  var items = '';
  for (let item of set)
    items += item + ',';
  if (items.length > 0)
    items = items.substring(0, items.length-1);
  items = '[' + items + ']';
  return items;
}

export function formatItems(items, divider) {
  var result = null;
  if (items.length === 0) { result = ''; }
  else {
    result = '';
    for (let item of items)
      result += item + divider;
    result = result.substring(0, result.length-divider.length);
  }
  return result;
}

export function addBlockItem(items, block) {
  var result = [];
  for(let item of items) {
    result.push(item);
    result.push(block);
  }
  return result;
}

export function getSelectedText(elementId) {
  var elt = document.getElementById(elementId);
  if (elt.selectedIndex == -1)
      return null;
  return elt.options[elt.selectedIndex].text;
}

export function isUndefined(element) {
  return (element === undefined);
}

export function isNull(element) {
  return (element === null);
}

export function hasValue(element) {
  return (!isUndefined(element) && !isNull(element));
}

export function MessageException(title, message) {
  this.title = title;
  this.message = message;
}