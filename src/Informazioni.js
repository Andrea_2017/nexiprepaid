import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cardType: 'contrattualizzata'
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      cardType: event.target.value
    });
  }
	
  handleSubmit(event) {
    event.preventDefault();
  }
  
  render() {
    const cardType = this.state.cardType;
	  return (
      <div>
        <div>
          <input type='radio' onChange={this.handleChange} name='cardtype' value='contrattualizzata' checked={(cardType === 'contrattualizzata') ? true : false} />Carta contrattualizzata
        </div>
        <div>
          <input type='radio' onChange={this.handleChange} name='cardtype' value='contrattualizzataMinore' checked={(cardType === 'contrattualizzataMinore') ? true : false} />Carta contrattualizzata per minore
        </div>
        <div>
          <input type='radio' onChange={this.handleChange} name='cardtype' value='nonContrattualizzata' checked={(cardType === 'nonContrattualizzata') ? true : false} />Carta non contrattualizzata (max 3 a persona)
        </div>
        <p>
          <Link to={'/richiesta?type='+cardType} className='fright'>Conferma</Link>
        </p>
      </div>
	  );
  }
}

class InformazioniWoRouter extends React.Component {
  constructor(props) {
    super(props);
    const { match, location, history } = this.props;
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    console.log(this.props.location);
    return (
      <div className='column columnTight'>
        <h2>Informazioni</h2>
      </div>
    );
  }
}

export const Informazioni = withRouter(InformazioniWoRouter)